(* ::Package:: *)

(* Dependencies *)
(* Symbols from other packages are prefixed w/ package name *)
Needs["Utilz`"]
Needs["MathSbmlUtilz`"]
Needs["MathCrn`"]

(* Mathematica Package *)
BeginPackage["SbmlReacRate`"]
(* Exported symbols added here with SymbolName::usage *)

SbmlReacRate::nsbml = "[ERROR] Unsupported or not a valid SBML model: `1` ."
SbmlReacRate::addrr = "[WARN] Expansion of the rate law of the following reactions is additive: `1`. These reactions may be either reversible (despite being defined otherwise) or stochastic, but it also may indicate, e.g., an explicit variable substitution."



(********************************* MONOTON ************************************)

SbmlRatesJacobian::dryr = "[INFO] This is a dry run, symbolic computation of derivatives is suppressed."

SbmlRatesJacobian::usage = "SbmlRatesJacobian[modPath, ?context, ...]
     Returns list with `RatesJacobian` from `MathCrn` and Jacobian
     matrix headings, i.e. list of row names (reactions' IDs) and column names
     (species' IDs).

    `modPath` [`String`]:  SBML file path or url (cf. `ReadModel` from
                           `MathSbmlUtilz`)
    `context` [`String`]:  Local context for a model variables etc. (by default
                           model's SBML ID)


SbmlRatesJacobian[mod, ...]

    `mod` [`MathSBML` model]:   Must be read with `SBMLStoichiometryMatrix`
                                return option.

Options::
    DryRun -> True
        Return an array of `Missing` entries. This allows to perform all the
        checks, w/o the actual possibly long computation.

    `RreComponents` options, e.g. RemoveUnconnectedSpecies.
    `RatesJacobian` options, e.g. Method.


Examples::
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\";
    mod = ReadModel[modUrl, context -> \"file\", return -> {Global`SBMLStoichiometryMatrix -> True}];
    tabf = TableForm[#[[1]], TableHeadings -> #[[2]]] &;
    SbmlRatesJacobian[modUrl, \"url\"] // tabf
    SbmlRatesJacobian[mod] // tabf

    (* N.b.: normal Jacobian does not take into account that S is an
    input / constant here, however restricted does due to the single variable
    conservation law corresponding to a null row in the stoichiometry matrix *)
    SbmlRatesJacobian[mod, Method -> \"Full\", RemoveUnconnectedSpecies -> False] // tabf
    SbmlRatesJacobian[mod, RemoveUnconnectedSpecies -> False] // tabf

    (* Dry run message and result *)
    SbmlRatesJacobian[mod, DryRun -> True] // tabf
"



SbmlRatesMonotonicityTab::irr = "[ERROR] Reactions in the model are not all irreversible."
SbmlRatesMonotonicityTab::nass = "[INFO] Dropping assignments which evaluated to numerical values: `1`."

SbmlRatesMonotonicityTab::usage = "SbmlRatesMonotonicityTab[...]
    Return sign pattern of a stoichiometric subspace restricted rates Jacobian
    of the SBML model, together w/ headers (reaction's IDs for rows and species
    IDs for columns); ready to `Export` e.g. in a tabular data format.
    Parameters and relelvant options are passed directly to the
    `SbmlRatesJacobian` function.

    To facilitate determining sign of the symbolic derivatives constraints and
    algebraic rules are directly used as assumptions, and right-hand sides of
    the initial assignments and assignment rules are assumed to be positive
    (together with all the variables appearing in derivatives expressions, so,
    in particular assignments left-hand sides).
    Remark: in contrast to the straightforward resolution of the assignments,
            currently employed \"weaker\" strategy seems to have a better chance
            at (correctly) determining the sign of a~rate derivative expression.

Options::
    AllIrreversible -> True
        Flag indicating whether all reactions in the model are required to be
        irreversible. Note that for reversible reactions the non-zero symbolic
        sign pattern will be most likely undefined (due to the top level \"-\"),
        for the variables dependent on rate variables via mass conservation
        equations.
    NumericAssignments -> False
        Flag indicating whether to take into account assignment rules or initial
        assignments which right-hand side evaluated to a numerical value.
        Dropped assignments are reported within the
        `SbmlRatesMonotonicityTab::nass` info symbol.
        Note: if this flag is set to `True` and a~numerical value is less or
        equal to zero you will get the `$Assumptions::fas` warning.


Examples::
    tab = SbmlRatesMonotonicityTab[\"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\"];
    tab // TableForm
    ExportString[tab,\"CSV\"]

    (* info/warning/error messages reporting *)
    Off[General::stop]
    ( Print[\"[INFO] Messages for: \", #];
      {#, SbmlRatesMonotonicityTab[#, \"m\", DryRun -> True]}
    ) & /@ {
    \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000001\" (* error: reversible *),
    \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000020\" (* error: not a CRN model *),
    \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000091\" (* warn: stochastic dimerisation rate law: n (n - 1) / 2 *),
    \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000093\" (* warn: reversible wrongly annotated as irreversible *),
    \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\" (* ok *),
    \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000507\" (* error: unsupported sbml level 3 *)
   } // TableForm
   On[General::stop]


    mod = ReadModel[\"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000356\", context -> \"m\", return -> {Global`SBMLStoichiometryMatrix -> True}];
    SbmlRatesMonotonicityTab[mod] // TableForm
    SbmlRatesMonotonicityTab[mod, NumericAssignments -> True] // TableForm
"



BatchSbmlRatesMonotonicity::ndir = "[ERROR] Can't access directory \"`1`\"."

BatchSbmlRatesMonotonicity::usage = "BatchSbmlRatesMonotonicity[modPathList,tabDirPath]
    Tries to compute `SbmlRatesMonotonicityTab` for every file indicated via
    path on `modPathList` and to save resulting table as an `CSV` file with
    the same name in the `tabDirPath`.

    Attention: unlike some other SBML file path-based functions this one does
               not work for URLs.

BatchSbmlRatesMonotonicity[modDirPath,tabDirPath]
    Like above but for every file w/ \".xml\" extension in the `modDirPath`.

Examples::
    spF[pref_] := StringJoin[pref, #] &
    modIds = spF[\"BIOMD0000000\"] /@ ToString /@ Range[306, 312]
    modUrls = spF[\"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=\"] /@ modIds;
    dirPath = CreateDirectory[]
    FileNames[\"*\", {dirPath}]

    ssF[suf_] := StringJoin[#, suf] &
    modPaths = FileNameJoin[{dirPath, #}] & /@ (ssF[\".xml\"] /@ modIds);
    modPaths = Thread[URLSave[modUrls, modPaths]];
    FileNames[\"*\", {dirPath}]

    (* models paths variant *)
    res1DirPath =
     CreateDirectory[FileNameJoin[{dirPath, \"1\"}]]
    res1Paths = BatchSbmlRatesMonotonicity[modPaths, res1DirPath]
    FileNames[\"*\", {res1DirPath}]
    FileByteCount /@ %

    (* models directory variant *)
    res2DirPath =
     CreateDirectory[FileNameJoin[{dirPath, \"2\"}]]
    res2Paths = BatchSbmlRatesMonotonicity[dirPath, res2DirPath]
    FileNames[\"*\", {res2DirPath}]
    FileByteCount /@ %

    DeleteDirectory[dirPath, DeleteContents -> True]
"



Begin["`Private`"]
(* Begin Private Context *)
(* OPT switch to Throw decorators for failing functions; cf. chainIfNotFailed in
       http://mathematica.stackexchange.com/questions/29321/what-are-the-best-practices-most-common-idiomatic-ways-to-report-errors-in-m
*)

nSbml[args__] := (Message[SbmlReacRate::nsbml,args]; $Failed)

readSbml[modPath_String, ctx_String:"m"] :=
    MathSbmlUtilz`ReadModel[modPath, context -> ctx,
            return -> {Global`SBMLStoichiometryMatrix -> True}]
readSbml[mod_?MathSbmlUtilz`ModelQ] := mod
readSbml[args__] := nSbml[args]

readSbmlFun[f_?Utilz`FunctionQ,args__,opts:OptionsPattern[]] :=
    (* OPT generalize to filter opts for ReadModel and add args to f *)
    With[ {mod=readSbml[args]}, If[mod === $Failed, Return[$Failed]]; f[mod,opts] ]

(********************************* MONOTON ************************************)
Options[SbmlRatesJacobian] := Utilz`OptsUnion[{
    DryRun -> False
}, Options[MathSbmlUtilz`RreComponents], Options[MathCrn`RatesJacobian]]
SbmlRatesJacobian[mod_?MathSbmlUtilz`CrnModelQ, opts:OptionsPattern[]] :=
    Module[{sm, xv, vv, headings},
        {sm, xv, vv, headings} =
            (*MathSbmlUtilz`RreComponents[mod];*)
            Utilz`ApplyOptSafe[MathSbmlUtilz`RreComponents, mod, None, opts];
        headings = Reverse@headings;

        If[OptionValue[DryRun],
            Message[SbmlRatesJacobian::dryr];
            Return[{ ConstantArray[Missing["Not computed"],Length/@headings], headings }]
        ];

        {
            Utilz`ApplyOptSafe[MathCrn`RatesJacobian, sm, vv, xv, opts],
            headings
        }
    ]
SbmlRatesJacobian[mod_?MathSbmlUtilz`ModelQ, ___] :=
    (Message[MathSbmlUtilz`CrnModelQ::fail]; $Failed)
SbmlRatesJacobian[args__] :=
    readSbmlFun[SbmlRatesJacobian,args]



maskPattern = {(True|False)...}
reactionIdsSelect[mod_?MathSbmlUtilz`ModelQ, m:maskPattern] :=
    Part[ MathSbmlUtilz`ReactionsIds[mod], Flatten[Position[m, True]] ]

irreversibleReactionsMask[mod_?MathSbmlUtilz`ModelQ] :=
    MathSbmlUtilz`IrreversibleReactionQ /@ (Global`SBMLReactions /. mod)

(* (opt) strengthen by checking for opposite signs of the sides; cf. BIOMD0000000036, DBT reaction *)
possReversibleReactionRateQ =
    Composition[MatchQ[#, Plus | Minus] &, Head, Expand]
possReversibleReactionsMask[mod_?MathSbmlUtilz`ModelQ, isIrrMask:maskPattern] :=
    Module[{addMask},
        addMask = possReversibleReactionRateQ /@
            (*MathSbmlUtilz`RreComponents[mod, RemoveUnconnectedSpecies->False, RemoveUnconnectedReactions->False][[3]];*)
            Last /@ MathSbmlUtilz`ResolvedReactionRates[mod];
        Thread[addMask && isIrrMask]
    ]
possReversibleReactionsMask[mod_?MathSbmlUtilz`ModelQ] :=
    possReversibleReactionsMask[mod, irreversibleReactionsMask[mod]]
possReversibleReactionsIds[mod_?MathSbmlUtilz`ModelQ, maskArgs___] :=
    Module[{possRev},
        possRev = reactionIdsSelect[mod,
            possReversibleReactionsMask[mod, maskArgs]
        ];
        If[!Utilz`EmptyListQ[possRev],
            Message[SbmlReacRate::addrr, possRev]
        ];
        possRev
    ];

addTableHeadings[tab_?MatrixQ,headings:{{__},{__}},rowNamesStr_:""] :=
    MapThread[Prepend, {
        Prepend[tab, Last@headings], Prepend[First@headings, rowNamesStr]
    }]

UndefinedSignString = "NA"
Options[SbmlRatesMonotonicityTab] := Utilz`OptsUnion[{
    AllIrreversible -> True,
    NumericAssignments -> False
}, Options[SbmlRatesJacobian]]
SbmlRatesMonotonicityTab[m_?MathSbmlUtilz`ModelQ,opts:OptionsPattern[]] :=
    Module[{isIrr,mod,temp,vJP,headings,ics,assigns,constrs,eqs,nAssignsIdx,assume,tab},

        isIrr = irreversibleReactionsMask[m];
        If[OptionValue[AllIrreversible] && !Fold[And, True, isIrr],
            Message[SbmlRatesMonotonicityTab::irr];
            Return[$Failed]
        ];
        (* report possibly wrongly annotated reversible reactions *)
        possReversibleReactionsIds[m, isIrr];

        (* can facilitate determination of the signs, especially assumptions *)
        (*mod = MathSbmlUtilz`ResolveInitialAssignments[m];*)
        mod = m;
        temp = Utilz`ApplyOptSafe[SbmlRatesJacobian, mod, None, opts];
        If[temp === $Failed, Return[$Failed]];
        {vJP,headings} = temp;

        (* attnetion: temp used all the way down this section *)
        temp = {
        	Global`SBMLInitialAssignments,
        	Global`SBMLAssignmentRules,
        	Global`SBMLConstraint,
        	Global`SBMLAlgebraicRules
        } /.mod;
        {ics,assigns,constrs,eqs} = If[ListQ[#],#,{}] & /@ temp;
        (* merge intial and persistent (rules) assignments *)
        assigns = Last /@ Join[ics, assigns];
        {assigns,constrs,eqs} = MathSbmlUtilz`ResolvedModelExpression[mod,
        	{assigns,constrs,eqs}, InitialAssignments -> True];
        (* drop & report numeric value assignments *)
        nAssignsIdx = Flatten@Position[assigns,_?NumericQ, {1}];
        If[Not[OptionValue[NumericAssignments] || Utilz`EmptyListQ[nAssignsIdx]],
            Message[SbmlRatesMonotonicityTab::nass,
            	Part[Join@@temp[[;;2]],nAssignsIdx]];
            assigns = assigns[[Complement[Range[Length[assigns]], nAssignsIdx]]];
        ];
        assume = Fold[And, True,
            Join[
                Thread[assigns > 0],
                constrs,
                eqs
            ]
        ];
        tab = MathCrn`SignPattern[vJP, assume];
        addTableHeadings[tab, headings, "ReactionID"]
    ]
SbmlRatesMonotonicityTab[args__,opts:OptionsPattern[]] :=
    readSbmlFun[SbmlRatesMonotonicityTab,args,opts]


dirQ[dirPath_String] :=
    Module[{isOk},
        isOk = DirectoryQ[dirPath];
        If[!isOk, Message[BatchSbmlRatesMonotonicity::ndir,dirPath]];
        isOk
    ]
sbmlRatesMonotonicityWrite[dir_String,fn_String, opts:OptionsPattern[]] :=
    Module[{fb,oldPP,ret},
        fb = FileNameJoin[{dir,FileBaseName[fn]}];
        (* full logging: repeat same messages and do not Short/Shallow them *)
        Off[System`General::stop];
        oldPP = System`$MessagePrePrint;
        Unset[System`$MessagePrePrint];
        ret = Utilz`LogOutput[fb<>".log", SbmlRatesMonotonicityTab, fn, opts];
        Set[System`$MessagePrePrint, oldPP];
        On[System`General::stop];
        If[ret === $Failed,
            Export[fb<>".txt", ret, "Text"],
            Export[fb<>".csv", Map[ToString,ret,{2}] ]
        ]
    ]
BatchSbmlRatesMonotonicity[modPathList_List, tabDirPath_?dirQ, opts:OptionsPattern[]] :=
        sbmlRatesMonotonicityWrite[tabDirPath, #, opts] & /@ modPathList
BatchSbmlRatesMonotonicity[modDirPath_?dirQ, args__] :=
        BatchSbmlRatesMonotonicity[FileNames["*.xml", modDirPath], args]
BatchSbmlRatesMonotonicity[__] := $Failed




End[] (* End Private Context *)

EndPackage[]
