(* ::Package:: *)

(* Dependencies *)
(* Symbols from other packages are prefixed w/ package name *)
Needs["Utilz`"]

(* Mathematica Package *)
BeginPackage["MathCrn`"]
(* Exported symbols added here with SymbolName::usage *)

(* General Utilz *)
(* TODO *)
FreeVariables::usage = "FreeVariables[expr]
    returns list of free (not bound) variables symbols in `expr`.
    Generalizes native `Variables` function.

    Oiriginal src: https://groups.google.com/d/msg/comp.soft-sys.math.mathematica/8GweRZp0HAo/szWGI4ysjhEJ .

    FIXME: not consistent, cf. `Subscript` vs. square brackets examples.


Examples::
    exv = {x, Subscript[x, i] + 1, x[i] + 1, Sin[x], Exists[y, Element[y, Reals], x*y == 1]};
    Variables /@ exv
    FreeVariables /@ exv
"

IndexedVect::usage = "IndexedVect[x, n]
    Creates a vector `x[1], x[2], ..., x[n]`, where `x` is a `Symbol`; cf.
    native `Indexed` function.


Examples::
    Thread[Indexed[x,Range[3]]]
    IndexedVect[\"x\",3]
"

Jac::usage = "Jac[f, x]
    Computes Jacobian matrix of a vector function `f` with respect to variables
    vector `x`.


Examples::
    Jac[{a r Cos[t], r Sin[t]}, {r, t}] // MatrixForm
    Simplify[Det[%]]
"

LinSubspaceJac::usage = "LinSubspaceJac[sm, ...]
	Components of the Jacobian matrix entries (computed from `...` arguments)
	corresponding to the linear subspace given by the column space of the `sm`
	matrix.

Examples::
    fv = {(x y)/(1 + x + y)}
    xv = FreeVariables[fv]
    (* regular non-restricted Jacobian *)
    (j = FullSimplify /@ Jac[fv, xv]) // MatrixForm
    sm = {{-1, 1}, {1, -1}};
    (* The 1-D linear subspace that the Jacobian is projected on *)
    Transpose[ColumnSpace[sm]]
    (* Jacobian component corresponding to linear subspace *)
    (rj = FullSimplify /@ LinSubspaceJac[sm, fv, xv]) // MatrixForm

    (* Countour plot of original function and:
        * gradients (blue vectors),
        * subspace restricted gradients (red vectors)
        * sample affine subspaces of projection (dashed red lines)
    *)
    pv = {{.7, 0}, {1.55, 0}};
    Show[
	    ContourPlot[fv, {x, 0, 1}, {y, 0, 1},
	        Epilog -> ({Red, Thick, Dotted,
	           Line[{#, First@Transpose[ColumnSpace[sm] + #]}]} & /@ pv)
        ],
	    VectorPlot[rj, {x, .05, .95}, {y, .05, .95}, VectorStyle -> Red],
	    VectorPlot[j, {x, .05, .95}, {y, .05, .95}]
    ]
"




SignPattern::ass = "[INFO] Assuming `1`."
SignPattern::usage = "SignPattern[m, ?assume]
    Gives sign pattern of a matrix `m`, i.e. matrix with -1, 0, +1, and
    `IndeterminateSign` entries, in case of, respectively, negative, zero,
    positive, and undetermined sign.

    Attention: currently assuming that all variables have positive values.
               Additional assumptions for `m` entries variables can be given via
               `assume` parameter (by default `True`).


Examples::
    Jac[{r Cos[t], r}, {r, t}] // MatrixForm
    SignPattern[%] // MatrixForm

"
IndeterminateSign::usage = "IndeterminateSign
    Symbol representing undetermined sign value.

Examples::
    (m = {{IndeterminateSign, x - y, 0}, {0, x, -y}}) // MatrixForm
    (q = SignPattern@m) // MatrixForm
    Unitize@q // MatrixForm
"
IndeterminateSignString::usage = "IndeterminateSignString
    `IndeterminateSign` symbol string representation.

Examples::
    ToString[IndeterminateSign]
    % == IndeterminateSignString
"
IndeterminateSignQ::usage = "IndeterminateSignQ[]
    Test for the undetermined sign elements in the sign pattern.

Examples::
    SignPattern[{{IndeterminateSign, x - y, 0}, {0, x, -y}}] // MatrixForm
    Map[IndeterminateSignQ, %, {2}] // MatrixForm
"
SignValues::usage = "SignValues
    List of sign values.

    Note: these are ordered by no (0), determined (-1 or +1) and undetermined
    sign.
"
SignQ::usage = "SignQ[]
    Test for sign value.

Examples::
    SignQ /@ {-1, 0, 1, IndeterminateSign, -1., 0., 1., .5, Inifity}
"
SignPatternQ::usage = "SignPatternQ[]
	Test for the sign pattern matrix.

Examples::
    SignPatternQ /@ {
        q = SignPattern[{{IndeterminateSign, x - y, 0}, {0, x, -y}}],
        q /. {1 -> 1.0, 0 -> 0.0, -1 -> -1.0},
        q /. {IndeterminateSign -> Infinity}
    }
    SignPatternQ /@ {
        IndeterminateSign,
        {-1, 1},
        {{}} (* ok - empty sign pattern *),
        {{{}}}
    }
"

SignPatternSummary::usage = "SignPatternSummary[q]
	Summary statistics for sign pattern `q`.

Examples::
    SignPattern[{{IndeterminateSign, x - y, 0}, {0, x, -y}}] // MatrixForm
    TableForm[SignPatternSummary[%], TableHeadings -> {\"Summary(Q(A))\", SignPatternSummaryHeading}]
"
SignPatternSummaryHeading::usage = "SignPatternSummaryHeading
	Brief description of the `SignPatternSummary` entries.

Examples::
    cf. `SignPatternSummary`
"

SignPatternsDifferenceSummary::usage = "SignPatternsDifferenceSummary[q1, q2]
	Difference at i1 and i2 is a number of positions at which in `q1` one has
	`SignValues` at	i1 while in `q2` one has `SignValues` at i2.

Examples::
    (q = {{IndeterminateSign, IndeterminateSign, 0}, {0, 1, -1}}) // MatrixForm
    qdstf = TableForm[#, TableHeadings -> {SignValues, SignValues}] &;
    SignPatternsDifferenceSummary[q, q] // qdstf
    SignPatternsDifferenceSummary[q /. 0 -> IndeterminateSign,  q /. IndeterminateSign -> 1] // qdstf
"


SignedGraph::udef = "[WARN] Edge sign is not well defined for `1`. Omitting "<>
                    "ill-defined edges."
(* TODO: examples *)
(* TODO: make removing of the undtermined sign edges optional ? *)
SignedGraph::usage = "SignedGraph[m, ?vars]
    For a square matrix `m` returns a signed graph corresponding to a sign
    pattern of that matrix, excluding diagonal (no self-loops).

    `IndeterminateSign` edges are reported and not included in the graph.

    Returns `WeightedAdjacencyGraph` where signs are stored as the `EdgeWeight`,
    which is also used for labeling edges. Missing edges correspond to
    undetermined or 0 signs and are storred as \[Infinity\] weights.

    Vertices labels can be optionally given by `vars` argument. By default
    vertex lables are `\"Name\"`, i.e. vertex number).
"
(* TODO *)
UndirectedSignedGraph::udef = "[WARN] Edge sign is not well defined for `1`. "<>
                              "Omitting ill-defined edges."
UndirectedSignedGraph::usage = "UndirectedSignedGraph[m, ?vars]
    Undirected verions of the `SignedGraph` function.
"

SpinAssignment::usage = "SpinAssignment[g, ?vIdx]
    Assigns spin (vertices sign) to a signed graph, according to sign of edges.
    Returns list of -1, +1, or `IndeterminateSign`, with the meaning,
    respectively, of +, -, or undetermined spin for the corresponding vertex
    from `VertexList`.

    If spin is well defined for all vertices it may only be the one found or its
    negative. Initial vertex given by index `vIdx` has +1 assigned.
    In case of ill-defined spins, set of undefined spin vertices may vary,
    depending on choice of the initial vertex.
"
SpinAssignedSignedGraph::usage = "TODO"


(* Main contents *)
StoichiometricMatrices::usage = "StoichiometricMatrices[crn]
    Returns left- and right-hand side stoichiometric matrices, L and R
    respectively, of a chemical reaction network `crn`. The latter is expected
    in a form of a list of rules or pairs of up to first order polynomials.
    In a matrix form we have:
        x.L -> x.R,
    where x is a vector of species variables.
    Function works by extracting polynomial coefficients.

StoichiometricMatrices[crn, vars]
    Version with species variables explicitly given, not computed by
    `FreeVariables` function.


Examples::
    crn = {a + b -> 2 a, 2 a -> a + b, Null -> b};
    MatrixForm /@ ({lS, rS} = StoichiometricMatrices[crn])
    StoichiometricMatrix[crn] // MatrixForm
    xv = FreeVariables[crn];
    MapThread[Rule, {xv.lS, xv.rS}]
    (* Notice 0 instead of Null *)
"
StoichiometricMatrix::usage = "StoichiometricMatrix[crn]
    Convenience function that returns stoichiometric matrix of a chemical
    reaction network, i.e. difference of right- and left-hand side
    stoichiometric matrices.
    See `StoichiometricMatrices` function for details.

StoichiometricMatrix[L, R]
    Returns R-S.


Examples::
    See `StiochiometricMatrices`.
"

MCLaws::usage = "MCLaws[crn, ?const]
    Returns stoichiometric mass conservation equations (laws) for a given `crn`.
    Stoichiometric mass conservation equations are linear functions of species
    variables constant throught evolution of the underlying dynamical system.
    Technical description with meaning of `const` arg below.
    Attention: does not guarantee that `const` variables represent non-negative
    values, however, there's a workaround implemented for that (cf. examples).

MCLaws[S, ?const, ?vars]
    Returns dot product of `S` matrix left-null space elements and `vars` vector
    set equal to vector of indexed constant symbol `const`.


Examples:
    crn = {2 a -> b, b -> 2 a, b -> c};
    MCLaws[crn]
    StoichiometricMatrix[crn] // MatrixForm
    MCLaws[%, c]

    crn = {a1 -> a2, a2 -> a1, b1 -> b2, b2 -> b1, c1 -> c2, c2 -> c1};
    MCLaws[crn] // TableForm
    (* How RHS would look like w/o the workaround for non-negativeness *)
    NullSpace[Transpose[StoichiometricMatrix[crn]]].FreeVariables[crn] // TableForm
"

MARates::usage = "MARates[crn]
    Returns vector of Mass action rates for given chemical reaction network
    `crn` i.e.:
        \*SubscriptBox[\(v\), \(j\)]=\*SubscriptBox[\(k\), \(j\)]\!\(
        \*SubsuperscriptBox[\(\[Product]\), \(i = 1\), \(n\)]
        \*SuperscriptBox[SubscriptBox[\(x\), \(i\)],SubscriptBox[\(l\), \(i, j\)]]\)
    where \*SubscriptBox[\(l\), \(i, j\)] are left-hand side stoichiometric coefficients.

MARates[lS,?vars]
    Same as above with `lS` being left-hand side stochiometric matrix and `vars`
    optional variables names (by default: `IndexedVect[x,Length[lS]])`).


Examples::
    crn = {a + b -> 2 a, 2 a -> a + b, Null -> b};
    MARates[crn]
    MARates[First[StoichiometricMatrices[crn]]]
"

RREqs::usage = "RREqs[crn, rates]
    Returns reaction rate equations system of first order ODE system.
    Attention: rates must be constructed as functions of `FreeVariables[crn]`.

RREqs[S, rates, ?vars]
    Attention: rates must be constructed in variables given in `vars` (`x[1],
    x[2], ..., x[n]` by default).


Examples::
    crn = {a + b -> 2 a, 2 a -> a + b, Null -> b};
    RREqs[crn, MARates[crn]]
    {lS, rS} = StoichiometricMatrices[crn];
    vv = MARates[lS] (* in x[1],x[2] variables by default *)
    RREqs[rS - lS, vv] (* also in x[1],x[2] variables by default *)
"



RatesJacobian::meth = "[ERROR] Unknown method option: `1`."

RatesJacobian::usage = "RatesJacobian[crn, rates]
    Returns a Jacobian of reaction `rates` projected onto arbitrary `crn`
    stiochiometry class or, equivalently, `rates` derivative in all directions
    given by any `crn` stoichiometry class (transformed back to original `crn`
    coordinates, i.e. species concentrations variables).
    Attention: rates must be constructed as functions of `FreeVariables[crn]`.


RatesJacobian[S, rates, ?vars]
    Attention: `rates` vector must be constructed in variables given in `vars`
    (`x[1], x[2], ..., x[n]` by default).


Options::
    Method -> \"Restricted\" (default), \"Full\"
        Compute Jacobian, respectively, restricted to stoichiometry classes
        (column space of stoichiometry matrix) or full.


Examples::
	crn = {a + b -> c, c -> a + b};
	rates = MARates[crn];
	{lS, rS} = StoichiometricMatrices[crn];

	(* error reporting *)
	RatesJacobian[crn, rates, Method -> \"Fullish\"]
	RatesJacobian[lS - rS, rates, {a, c}, Method -> \"Fullish\"]

	(* the expected LHS monotone dependency *)
	Simplify /@ RatesJacobian[crn, rates, Method -> \"Full\"] // MatrixForm
	SignPattern[%] // MatrixForm
	Simplify /@ RatesJacobian[lS - rS, rates, {a, b, c}, Method -> \"Full\"] // MatrixForm

	(* additional influences on rates as implied by the mass conservation laws *)
	MCLaws[crn, const] // MatrixForm
	Simplify /@ RatesJacobian[crn, rates] // MatrixForm
	SignPattern[%] // MatrixForm
	Simplify /@ RatesJacobian[lS - rS, rates, {a, b, c}] // MatrixForm
"



(*TODO*)
IsMonotone::usage = "IsMonotone[jacobian]
    [Work In Progress]
    Check if the ODE system with Jacobian `jacobian` is monotone.
"

ReduceOrder::nfrr = "[ERROR] Not a full rank reduction to variables `1`"<>
					",  i.e. Rank[S1] != Rank[S]."

(*TODO*)
ReduceOrder::usage = "ReduceOrder[S, varsAll, varsKeep]
    [Work In Progress]
    Reduce stoichiometric matrix `S` of a CRN by columns. Variables
    `varsAll` correspond to `S` columns, and `varsKeep` indicate
    columns to reduce `S` to, such that:

		S2 == L.S1
		x2 == c + L.x1

    where `S1` and ``x1` are columns and coordinates corresponding to
    the kept variables, `S2` and  `x2` are the remaining columns and
    coordinates, `L` is the link matrix, and `c` is a vector of
    constants.

    `S1` induced by `varsAll` is required to be of a rank of `S`,
    otherwise the method returns `$Failed`. If reduction to `varsAll`
	is valid, returns rules list with `{Vars1,Vars2,Idx1,Idx2,Sm1,Sm2,Lm}`,
    keys corresponding to variables names and indices, and stoichoimetric
	matrices of full and reduced parts, as well as the link matrix.

    Note: keys of the returned rules are defined in the context of the
          call of the function (`$Context`).


Examples::
	crn = {2 a\[Rule]b, b\[Rule]2 a};
	sm = StoichiometricMatrix[crn];
	xv = FreeVariables@crn;
	{vars1,vars2,L} = {Vars1,Vars2,Lm}/.ReduceOrder[sm,xv,{b}];
	Thread[Rule[vars2,IndexedVect[c,Length[vars2]]+L.vars1]] // MatrixForm
	Solve[MCLaws[sm,c,xv],vars2]//MatrixForm

	crn={s+e\[Rule]c,c\[Rule]s+e,c\[Rule]p+e};
	sm = StoichiometricMatrix[crn];
	xv = FreeVariables@crn;
	{vars1,vars2,L,idx1,S1} = {Vars1,Vars2,Lm,Idx1,Sm1}/.ReduceOrder[sm,xv,{s,p}];
	(mcr1 = Thread[Rule[vars2,IndexedVect[c1,Length[vars2]]+L.vars1]]) // MatrixForm
	(mcr2 = Flatten[Solve[MCLaws[sm,c2,xv],vars2]]) // MatrixForm
	rre = RREqs[crn, MARates[crn]];
	(* Check: reduce ODEs vs. reduce S *)
	Last/@rre[[idx1]]\[Equal]S1.MARates[crn]
	(Expand/@(rre[[idx1]] /. #))&/@{mcr1,mcr2}//MatrixForm
	(* Check: not a full rank reduction *)
	ReduceOrder[sm,xv,{c,e}]===$Failed
"



Begin["`Private`"]
(* Begin Private Context *)
idOrDefault[x_, y_] :=
    If[x === Null, y, x]
idOrXn[vars_, n_Integer, ctx_String, xName_String:"x"] :=
    idOrDefault[vars, IndexedVect[Symbol[ctx<>xName], n]]



FreeVariables[expr_] :=
    Union[Cases[
        Resolve[expr],
        (*_Symbol?(FreeQ[Attributes[#], Protected] &),*)
        (x_Symbol | x_Symbol[___]) /; (FreeQ[Attributes[x], Protected]) (*-> x*),
        {0,Infinity}(*, Heads -> True*)
    ]]

IndexedVect[x_Symbol, n_?NumberQ] :=
    Table[x[i], {i, n}];

Jac[fv_?VectorQ, xv_?VectorQ] :=
    Outer[D, fv, xv]

LinSubspaceJac[sm_?MatrixQ, jacArgs__] :=
    Module[{at},
        at = Orthogonalize[Transpose[Utilz`ColumnSpace[sm]]];
        Jac[jacArgs].(Transpose[at].at) (* brackets because you don't want to mess w/ symbolic numbers too fast *)
    ]



IndeterminateSignString = "NA"
Unprotect[IndeterminateSign]
IndeterminateSign /: ToString[IndeterminateSign] := IndeterminateSignString
IndeterminateSign /: Unitize[IndeterminateSign] := 1
Protect[IndeterminateSign]
IndeterminateSignQ = SameQ[IndeterminateSign,#] &

SignValues = {0, -1, 1, IndeterminateSign}
(* Rationalize for consistency; cf. BM308 r5 rate deriv wrt Y *)
SignQ = MemberQ[SignValues, Rationalize[#]] &
SignPatternQ = MatrixQ[#,SignQ] &

SignPattern[m_?MatrixQ, assume_:True] :=
    Module[{ass, q},
        (* OPT include possibility of dropping the positiveness assumption *)
        ass = assume && Fold[And, True, Thread[FreeVariables[m] > 0]];
        If[ass =!= True, Message[SignPattern::ass,ass]];
        q = Assuming[ass, Map[Composition[Simplify, Sign], m, {2}]];
        Map[If[SignQ[#], #, IndeterminateSign] &, q, {2}]
    ]


SignPatternSummaryHeading = {"NRow","NCol","NNzero","NUdet"}
SignPatternSummary[q:_?SignPatternQ] :=
    (* Remember: sync w/ SignPatternSummaryHeading *)
    Join[
    	Dimensions[q],
    	{Total[Unitize@q,2], Count[q, _?IndeterminateSignQ, 2]}
    ]



nSquareMatrixQ[n:_?Utilz`NonNegativeIntegerQ] :=
    (SquareMatrixQ[#] && (Length[#] == n)) &
qdsQ = nSquareMatrixQ[Length[SignValues]]
qdsKeyToIdx = Dispatch[Thread[SignValues -> Range[Length[SignValues]]]]
qdsConst[c:_:0] := ConstantArray[c,Table[Length@SignValues,{2}]]
SignPatternsDifferenceSummary[q1:_?SignPatternQ, q2:_?SignPatternQ] := (
    Module[{q2Part,qds},
    	q2Part = Part[q2,##] &;
    	qds = qdsConst[0];
        MapIndexed[
        	(qds[[#1 /. qdsKeyToIdx, (q2Part@@#2) /. qdsKeyToIdx]] += 1 &),
        	q1,{2}
    	];
        qds
    ]
) /; SameQ @@ (Dimensions /@ {q1,q2})
SignPatternsDifferenceSummary::ish = " Sign pattern matrices have to have"<>
                                       " same dimensions."
SignPatternsDifferenceSummary[q1:_?SignPatternQ, q2:_?SignPatternQ] := (
    Message[Utilz`UtilzError::ish,SignPatternsIntersection::ish]; $Failed
)



signedGraphMatrix[m_, vl_] :=
    (* vl:  vertex labels list for reporting inconsistencies *)
    Module[{q, udefEdgeIdx},
        q = SignPattern[m];
        (* TODO: extend to non-square matrices (assume a bipartiate graph;
                 cf. GraphLayout -> "BipartiteEmbedding" option) *)
        (* report undefined edges and remove them *)
        udefEdgeIdx = Complement[
            Position[q, IndeterminateSignQ, {2}],
            Table[{i,i},{i,Range[Length[q]]}]
        ];
        If[Length[udefEdgeIdx] > 0,
            Message[ SignedGraph::udef,
                Thread[List[
                    DirectedEdge @@@ (vl[[#]] & /@ udefEdgeIdx),
                    (m[[##]] &) @@@ udefEdgeIdx]]
            ]
        ];
        q = q /. {IndeterminateSign -> 0};
        (* rm self loops *)
        q - DiagonalMatrix[Diagonal[q]]
    ]
genVertexLabels[vl_] :=
    Thread[ Range[Length[vl]] ->  vl ]
SignedGraph[m_?SquareMatrixQ, vars_:Null] :=
    Module[{q, vl},
        vl = idOrDefault[vars, Range[Length[q]]];
        q = signedGraphMatrix[m, vl];

        WeightedAdjacencyGraph[q /. {0 -> Infinity}, DirectedEdges -> True,
            EdgeLabels -> "EdgeWeight", VertexLabels -> genVertexLabels[vl]
        ]
    ]
UndirectedSignedGraph[m_?SquareMatrixQ, vars_:Null] :=
    Module[{q,qU,qL,vl,udefEdgeIdx},
        vl = idOrDefault[vars, Range[Length[q]]];
        q = signedGraphMatrix[m, vl];

        (* make sign pattern matrix symmetric (undirected) *)
        {qU, qL} = UpperTriangularize /@ {q, Transpose[q]};
        (* check & report edge sign conlficts *)
        udefEdgeIdx = Position[qU*qL, x_ /; x < 0, 2];
        If[ Length[udefEdgeIdx] > 0,
            Message[UndirectedSignedGraph::udef,
                UndirectedEdge @@@ (vl[[#]] & /@ udefEdgeIdx)]
            (* nothing to be done, inconsistent signs will cancel each other out *)
        ];
        q = MapThread[Sign[#1 + #2] &, {qU, qL} , 2];

        WeightedAdjacencyGraph[ q /. {0 -> Infinity}, DirectedEdges -> False,
            EdgeLabels -> "EdgeWeight",
            VertexLabels -> genVertexLabels[vl]
        ]
    ]


getVLab[g_,v_] := PropertyValue[{g, v}, VertexLabels]
vLabEdge[g_, e_?({_,_} || _UndirectedEdge)] := UndirectedEdge @@ (getVLab[g,#]& /@ e)
SpinAssignment[g_,vIdx_:1] :=
    (* depth first scan with sign (spin) assignment *)
    (* FIXME: unnecessarily going twice via every undirected edges (Mathematica DFS implementation seems v. buggy) *)
    Module[{
        sa, getESign, getVSign, setVSign,
        undiscoveredVertexEdgeFun, discoveredVertexEdgeFun
    },
        sa = ConstantArray[0,VertexCount[g]];
        getESign[e_] := (PropertyValue[{g, e}, EdgeWeight]);
        getVSign[v_] := (sa[[v]]);
        setVSign[v_, s_] := (
            (*Print["Setting ",getVLab[g,v]," sign to ", s];*)
            sa[[v]] = s;
            (*Print["sign(",getVLab[g,v],")=", getVSign[v]];*)
        );
        setVSign[vIdx,1];

        undiscoveredVertexEdgeFun[e_] := Block[{eSign},
           (* update current vertex sign/spin *)
           eSign = getESign[e];
(*Print["Visiting ", getVLab[g,e[[2]]], " via edge ", vLabEdge[g,e],
    " w/ sign=", eSign, " and vertex sign=", getVSign[First[e]],"."];*)
           setVSign[Last[e], getESign[e]*getVSign[First[e]]];
        ];
        discoveredVertexEdgeFun[e_] := Block[{cvSign, v},
           (* check if vertex sign/spin agrees and if not set undefined vertex sign *)
           v = e[[2]];
           cvSign = getESign[e]*getVSign[First[e]];
(*Print["Revisiting ", getVLab[g,v], " via edge ", vLabEdge[g,e]," w/ sign=",
    getESign[e] ,". Spin should be ", cvSign, " and is ",  getVSign[v], "."];*)
           If[ getVSign[v] != cvSign, setVSign[v, Infinity]];
        ];
        DepthFirstScan[g, vIdx, {
            "FrontierEdge" -> undiscoveredVertexEdgeFun,
            "BackEdge" -> discoveredVertexEdgeFun ,
            "ForwardEdge" -> discoveredVertexEdgeFun,
            "CrossEdge" -> discoveredVertexEdgeFun
        }];
(*
Print[HighlightGraph[g,
    TreeGraph[VertexList[g], dfs (*, VertexLabels -> Thread[Range[Length[xv]] -> xv]*)],
    VertexSize -> {1 -> 0.1}
]];
*)
        sa
   ]
SpinAssignedSignedGraph[g_, sa_List] :=
    WeightedAdjacencyGraph[Normal[WeightedAdjacencyMatrix[g]] /. {0->Infinity},
        EdgeLabels -> "EdgeWeight", VertexLabels -> Thread[VertexList[g] -> sa]
    ] /; VertexCount[g] == Length[sa]



StoichiometricMatrices[crn_, vars_: Null] :=
    Module[ {xv, lsm, rsm},
        xv = idOrDefault[vars, FreeVariables[crn]];
        (* first order coefficients; applied to all reactions sides *)
        {lsm, rsm} =
         Transpose /@
          MapThread[Normal[CoefficientArrays[{##}, xv][[2]]] &,
           crn /. Rule -> List];
        {lsm, rsm}
    ]
StoichiometricMatrix[lsm_?MatrixQ,rsm_?MatrixQ] :=
    rsm - lsm
StoichiometricMatrix[crn_] :=
    StoichiometricMatrix@@StoichiometricMatrices[crn]

MCLaws[sm_?MatrixQ, const_Symbol: Null, vars_: Null] :=
    Module[ {ctx, xv, cv, q, nq},
        ctx = $Context;
        xv = idOrXn[vars, Length[sm], ctx];
        cv = IndexedVect[ idOrDefault[const, Unique[ctx<>"$"]],
            Length[xv]-MatrixRank[sm] ];
        q = NullSpace[Transpose[sm]];
        If[Utilz`EmptyListQ[q], Return[{}]];
        (* quick fix for a natural assumption that cv vars are non-negative:
           kernel matrix version that minimizes number of negative entries
           TODO: test me on an example w/ tot = a - (b+c) conservation law
         *)
        nq = -q;
        q = If[#1 < #2 & @@ (Length[Position[#, x_ /; x < 0, 2]] & /@ {q, nq}),
        	q, nq];
        Thread[cv == q.xv]
    ]
MCLaws[crn_, const_Symbol: Null] :=
    MCLaws[StoichiometricMatrix[crn], const, FreeVariables[crn]]

MARates[lsm_?MatrixQ (*: {{__?NumberQ} ..}*), vars_: Null] :=
    Module[ {n, m, ctx, xv, kv},
        ctx = $Context;
        {n,m} = Dimensions[lsm];
        xv = idOrXn[vars, n, ctx];
        kv = IndexedVect[Symbol[ctx<>"k"], m];
        kv*(Fold[Times, 1, #] & /@ (xv^# & /@ Transpose[lsm]))
    ]
MARates[crn_] :=
    MARates[First[StoichiometricMatrices[crn]], FreeVariables[crn]]

(* TODO: add option for argument-indexed vars, i.e. x[t];
         then D[#,# /. {_[v_] -> v}] & should be mapped over xv *)
RREqs[sm_?MatrixQ, rates_, vars_:Null] :=
    Module[ {xv},
        xv = idOrXn[vars, Length[sm], $Context];
        Thread[Derivative[1] /@ xv == sm.rates]
    ]
RREqs[crn_, rates_] :=
    RREqs[StoichiometricMatrix[crn], rates, FreeVariables[crn]]


Options[RatesJacobian] := {
    Method -> "Restricted"
}
(* FIXME? does not work for ommited vars argument? *)
RatesJacobian[smOrCrn_, rates_, opts:OptionsPattern[]] :=
    RatesJacobian[smOrCrn, rates, Null, opts]
RatesJacobian[sm_?MatrixQ, rates_, vars_, opts:OptionsPattern[]] :=
    Module[ {xv, meth, jacF},
        xv = idOrXn[vars, Length[sm], $Context];
        meth = OptionValue[RatesJacobian,opts,Method];
        jacF = Switch[meth,
        	"Restricted", LinSubspaceJac,
        	"Full", (* drop stoichiometry matrix *) Jac[#2,#3] &,
        	_, (Message[RatesJacobian::meth, meth]; $Failed &)
    	];
        jacF[sm, rates, xv]
    ]
RatesJacobian[crn_, rates_, vars_, opts:OptionsPattern[]] :=
    RatesJacobian[
    	StoichiometricMatrix[crn],
    	rates,
    	idOrDefault[vars,FreeVariables[crn]],
    	opts
	]

IsMonotone[j_] :=
    Module[{ujG,jSA},
        ujG = UndirectedSignedGraph[Transpose[j]];
        jSA = SpinAssignment[ujG];
        {
            Length[Position[jSA, x_ /; Abs[x] != 1, 1, 1]] == 0,
            SpinAssignedSignedGraph[ujG,jSA]
        }
    ]

(*TODO: DEBUG option for printing *)
ReduceOrder[sm_, xv_, x1_] :=
    Module[ {x2, ix1, ix2, sm1, sm2, lm},
		ix1 = Flatten[Position[xv, #] & /@ x1];
		sm1 = sm[[ix1, ;;]];
(*
Print["[DEBUG] {\!\(\*SuperscriptBox[\(x\), \(1\)]\),\!\(\*SuperscriptBox[\(S\), \(1\)]\)}:"];
Print[MatrixForm /@ {x1, sm1}];
*)
        If[MatrixRank[sm] != MatrixRank[sm1],
			Message[ReduceOrder::nfrr,x1];
			Return[$Failed]              
        ];
        x2 = Complement[xv, x1];        
        ix2 = Flatten[Position[xv, #] & /@ x2]; (*Complement[Range@Length@xv,ix1];*)
        sm2 = sm[[ix2, ;;]];
(*
Print["[DEBUG] {\!\(\*SuperscriptBox[\(x\), \(2\)]\),\!\(\*SuperscriptBox[\(S\), \(2\)]\)}:"];
Print[MatrixForm /@ {x2, sm2}];
*)
        lm = Transpose[LinearSolve[Transpose[sm1], Transpose[sm2]]];
(*
Print["[DEBUG] L:"];
Print[MatrixForm[lm]];
Print["[DEBUG] Sanity check: link matrix \!\(\*SuperscriptBox[\(S\), \(2\)]\)=L.\!\(\*SuperscriptBox[\(S\), \(1\)]\).. " <> If[ lm.sm1 == sm2,"PASS","FAIL" ]];
*)
		ctx = $Context;
        {
			Symbol[ctx<>"Vars1"]->x1, Symbol[ctx<>"Vars2"]->x2,
			Symbol[ctx<>"Idx1"]->ix1, Symbol[ctx<>"Idx2"]->ix2,
			Symbol[ctx<>"Sm1"]->sm1, Symbol[ctx<>"Sm2"]->sm2,
			Symbol[ctx<>"Lm"]->lm
		}
    ]

End[] (* End Private Context *)

EndPackage[]
