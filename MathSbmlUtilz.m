(* ::Package:: *)

(* Dependencies *)
(* Symbols from other packages are prefixed w/ package name *)
Needs["MathSBML`"]
(*Needs["Units`"]*)
Needs["Utilz`"]

(* Mathematica Package *)
BeginPackage["MathSbmlUtilz`"]
(* Exported symbols added here with SymbolName::usage *)

(*
Molar::usage = "Molar
    Molar unit defined as a Mole / Liter, using `Units` package.

Examples::
    ...
"
*)

(* (OPT) additional `Dispatch` wrapper for performance if needed *)
ReadModel::usage = "ReadModel[pathOrUrl, opt1, ...]
    Essentially `SBMLRead` from `MathSBML` pkg but  `$Context` safe and w/
    possibility of giving URL instead of file path (URL version is used when
    `URLQ` is `True`). In the latter case, `URLSave` options are also
    accepted (default read timeout is set by default to 3 second).

    Attention: `MathSBML` uses `\"Global`\"` namespace for its symbols. When
       using private context add `\"Global`\"` context explicitly to `MathSBML`
       symbols which you use in input options, such that you avoid a clash with
       local symbols that would be created during the function call otherwise
       (cf. `SBMLStoichiometryMatrix` usage in Examples). Additionally, you can
       prepend `\"Global`\"` context to `$ContextPath` to avoid later explicit
       usage of the `\"Global`\"` context with `MathSBML` defined key symbols
       such as `SBMLConstants` (cf. Examples).

Examples::
    SetOptions[EvaluationNotebook[], CellContext -> Notebook]
    $Context
    $ContextPath = DeleteCases[$ContextPath, \"Global`\"]
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\";
    mod = ReadModel[modUrl, context -> \"m\",
       return -> {Global`SBMLStoichiometryMatrix -> True}];
    {
      {Global`SBMLContext, Global`SBMLStoichiometryMatrix},
      {SBMLContext, SBMLStoichiometryMatrix}
      } /. mod
    PrependTo[ $ContextPath, \"Global`\"]
    {
      {Global`SBMLContext, Global`SBMLStoichiometryMatrix},
      {SBMLContext, SBMLStoichiometryMatrix}
    } /. mod
"

ExpectedModelKeys::usage = "ExpectedModelKeys
    Fixed list of \"SBML*\" keys expected from a `MathSBML` model.
"
ContainsExpectedModelKeysQ::usage = "ContainsExpectedModelKeysQ[keys]
    This test is based on a check for `ExpectedModelKeys`.
    The test result is cached via `DownValues`.
"
ModelQ::nkey = "[INFO] Not a MathSBML model. The following keys are not avaialable: `1`."
ModelQ::usage = "ModelQ[expr]
    Gives True if expr is a `MathSBML` model, and False otherwise.
    This test is based on list of rules form and `ContainsExpectedModelKeysQ`
    test.

Examples::
    mod = ReadModel[\"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000014\"];
    DownValues[ContainsExpectedModelKeysQ] // TableForm
    Timing /@ Table[ ModelQ[mod], {i, 2}]
    Timing /@ Table[ ModelQ[{a -> b}], {i, 2}]
    DownValues[ContainsExpectedModelKeysQ] // TableForm
"

CrnModelQ::fail = "[ERROR] Model is not in a chemical reactions network form."
CrnModelQ::usage = "CrnModelQ[expr]
    Gives True if expr is a `MathSBML` model in a chemical reaction network
    form, i.e. not a rate rules based one (explicit ODEs).

Examples::
    mods = ReadModel /@ {
        \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000020\",
        \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000093\",
        \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000400\"

    };
    {Global`SBMLModelName[#], CrnModelQ[#]} & /@ mods //TableForm
"

IrreversibleReactionQ::usage = "IrreversibleReactionQ[reac]
	Tests whether `reac` is irreversible.

Examples::
    ...
    mod = ReadModel[...]
    IrreversibleReactionQ /@ (Global`SBMLReactions /. mod)
"

ModelParameterQ::fail = "[ERROR] Not a model parameter: \"`1`\"."
ModelParameterQ::usage = "ModelParameterQ[mod][par]
    Test factory which given the model `mod` returns a test to verify wether
    `par` symbol or string is this model's parameter (in a contex independent
    fashion).

    Beware: assert that the `ModelParameterQ` factory function has been
            evaluated for a given model, e.g. when using it as a function
            argument.

Examples::
	modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\";
	mod = ReadModel[modUrl, context -> \"m\",
	   return -> {Global`SBMLStoichiometryMatrix -> True}];
	Global`SBMLParameters /. mod
	ModelParameterQ[mod] /@ {k1, m`k1, m`k5, \"k1\", \"k5\", env}
"
ModelConstantQ::fail = "Not a model constant: \"`1`\"."
ModelConstantQ::usage = "ModelConstantQ[mod][const]
	Cf. `ModelParameterQ`.

	Constants set in `MathSBML` is a set of parameters that have value
	assigned and evaluated as well as constants outside of the SBML parameters
	list such as the environment constant.

    `SBMLConstants` is a list of rules w/ LHS constants symbols and RHS numeric
    values, while `SBMLParameters` contains parameters strings only.

	Beware: A parameter that in SBML has the \"constant\" attribute set to
            \"true\", but has no value assigned will no be treated as a constant
            by `MathSBML`. I believe there is no way to retrive that information
            once the model has been read.


Examples::
	...
	Global`SBMLConstants /. mod
	ModelConstantQ[mod] /@ {k1, k5, env}
"
ModelSymbolicParameterQ::fail = "\"`1`\" is either not a model parameter or "<>
                                "it is a model constant (i.e. it has already "<>
                                "a numeric value assigned)."
ModelSymbolicParameterQ::usage = "ModelSymbolicParameterQ[mod][par]
    A `ModelParameterQ` that is not `ModelConstantQ`, i.e. has no numeric value.

Examples::
    cf. `ModelParameterQ` - all tests there should fail
"


ModelKeys::usage = "ModelKeys[mod]
	Returns list of `MathSBML` model keys, i.e. LHS symbols of `mod` rules list.

Examples::
    ...
    mod = ReadModel[...]
	ModelKeys[mod]
	Context /@ %
"

RmModelContextFromExpr::usage = "RmModelContextFromExpr[mod, expr]
    Removes context used in `mod` from `expr`. This is done via `StringReplace`
    function. `InputForm` is used for conversion of `expr` to a `String`. One
    can also provide an already converted expression.

    Beware: to avoid clush of system symbols `D`, `N`, `E`, `I`, and `Pi` they
            are substituted with corresponding double struck letter equivalents,
            i.e. \[DoubleStruckCapitalD], \[DoubleStruckCapitalN], \[DoubleStruckCapitalE], \[DoubleStruckCapitalI], and \[DoubleStruckCapitalP]i, defined in the \"Global`\" context.
            Also, you will run into errors if some of the symbols are already
            defined in your context.



Examples::
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000309\";
    mod = ReadModel[modUrl, context -> \"m\",
       return -> {Global`SBMLStoichiometryMatrix -> True}];
    SBMLFunctions /. mod // TableForm
    RmModelContextFromExpr[mod, %] // TableForm
    SBMLKineticLaws /. mod // TableForm
    (* Note the E variable substitution *)
    RmModelContextFromExpr[mod, %] // TableForm
"
AddModelContextToSym::usage = "AddModelContextToSym[mod, sym]
    Adds `mod` context to `sym`.

    Beware: does not convert back conflicting symbols substiutions done by
            `RmModelContextFromExpr`.

Examples::
    ...
    mod = ReadModel[...]
    AddModelContextToSym[mod, #] & /@ {\"x\", x, \"Global`x\", Global`x, \"\[DoubleStruckCapitalE]\", \[DoubleStruckCapitalE]}
    Context /@ %
"

ReactionsIds::usage = "ReactionsIds[mod]
    Returns list of SBML `String` identifiers for reactions defined in `mod`.

Examples::
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000001\";
    mod = ReadModel[modUrl];
    ReactionsIds[mod]
"
SpeciesIds::usage = "SpeciesIds[mod]
    Returns list of SBML identifiers for species defined in `mod`.

Examples::
    c.f `ReactionsIds`
"

GetConstantValue::usage = "GetConstantValue[mod, par]
    Returns the value of a model constant. Parameter `par` can be a symbol or
    a string, and it does not have to be prefixed with `mod` context.

Examples::
    cf. `SetSymbolicParameterValue`
"
SetSymbolicParameterValue::usage = "SetSymbolicParameterValue[mod, par, val]
    Sets a symbolic parameter `par` numeric value `val`. This makes `par`a non
    symbolic parameter, i.e. a model constant with `val` assigned. In turn, all
    instances of that parameter symbol within `mod` become substituted with
    `val`. A modified model is returned as a result.

    Beware: to be able to set a parameter value do not set its value in the SBML
            file (ommit a \"value\" attribute in a corresponding <parameter> tag).
            This will make the parameter a symbolic parameter. Otherwise, the
            parameter value is substituted into model equations while reading SBML.

Examples::
	modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\";
	mod = ReadModel[modUrl];
	SBMLConstants /. mod
	SBMLParameters /. mod
	GetConstantValue[mod, #] & /@ {env, k1, k2, k3, k4, k5, tau}

	PrettyPrintReactions[mod]
	PrettyPrintReactions[SetSymbolicParameterValue[mod, k1, 1]]
	SetSymbolicParameterValue[mod, env, 10]


	(* save the file and remove all \"value\" attributes from the SBML list of parameters; then ... *)
	modPath = FileNameJoin[{..., \"BIOMD0000000312.xml\"}]
	mod = ReadModel[modPath];

	SBMLConstants /. mod
	SBMLParameters /. mod
	PrettyPrintReactions[mod]
    ModelSymbolicParameterQ[mod] /@ {k1, k2}
	GetConstantValue[mod, k1]

	mod2 = SetSymbolicParameterValue[mod, k1, 10];
	SBMLConstants /. mod2
	SBMLParameters /. mod2
	PrettyPrintReactions[mod2]
	ModelSymbolicParameterQ[mod2] /@ {k1, k2}
	GetConstantValue[mod2, k1]
"

ResolveInitialAssignments::usage = "ResolveInitialAssignments[mod]
    Resolves initial assignments (IAs) and substitutes resolved values within
    the whole model, as well as moves all IAs that resolved to numerical values
    to list of constants or initial conditions.
    Initial assignment is considered resolved to a numerical value, when after
    replacement with constants and then after repeated replacement of the right
    hand side of all IAs, the righ hand side passes `NumericQ` test.


Options::
    MoveNotNumeric -> False
        Set to `True` to enforce moving IAs that did not resolved to numerical
        value constants or initial conditions lists.


Examples::
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\";
    mod = ReadModel[modUrl];
    {SBMLInitialAssignments,SBMLAssignmentRules,SBMLIC,SBMLConstants} /. #  & /@
    {
        mod,
        ResolveInitialAssignments[mod]
    }

    (* what have been exactly resolved +/- param indexing bug *)
    ias = SBMLInitialAssignments /. mod;
    Thread[(First /@ ias) -> ResolvedModelExpression[mod, Last /@ ias, Consts -> True, InitialAssignments -> True]] // TableForm
"

ResolvedModelExpression::usage = "ResolvedModelExpression[mod, expr]
    Returns `expr` resolved by default with `SBMLFunctions` (see options).

Options::
    Functions -> True
        Use SBML functions definitions to resolve `expr`.
    Consts -> False
        Use SBML constants definitions to resolve `expr`.
    InitialAssignments -> False
        Use SBML initial assignments to resolve `expr`.
    AssignmentRules -> False
        Use SBML assignment rules to resolve `expr`.


Examples::
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000079\";
    mod = ReadModel[modUrl];
    Transpose[{#, ResolvedModelExpression[mod, #]}] & @ (Global`SBMLKineticLaws /. mod) // TableForm


    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000309\";
    mod = ReadModel[modUrl, context -> \"m\", return -> {Global`SBMLStoichiometryMatrix -> True}];

    Global`SBMLAssignmentRules /. mod // TableForm
    Global`SBMLFunctions /. mod // TableForm

    vv = Global`SBMLKineticLaws /. mod;
    Transpose[{
       vv,
       ResolvedModelExpression[mod, vv],
       vv = ResolvedModelExpression[mod, vv, AssignmentRules -> True]}
    ] // TableForm

    (* sanity check: implicit dynamical system *)
    Thread[
        (Derivative[1] /@ (Global`SBMLSpecies /. mod))
        ==
        (Simplify /@ ((Global`SBMLStoichiometryMatrix /. mod).(Last /@ vv)))
    ] // TableForm
    (Global`SBMLODES /. mod) // TableForm


	mod = ReadModel[\"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\", context -> \"m\"];
	Global`SBMLInitialAssignments /. mod // TableForm
	ResolvedModelExpression[mod, m`R[0]]
	ResolvedModelExpression[mod, m`R[0], InitialAssignments -> True]

	(* it's just term replacement - use carefully; cf. ResolveInitialAssignments *)
	SBMLConstants /. mod // TableForm
	First[ ResolvedModelExpression[mod, SBMLIC /. mod, InitialAssignments -> True, Consts -> True] ]
	First[ SBMLIC /. ResolveInitialAssignments[mod] ]
"
ResolvedReactionRates::usage = "ResolvedReactionRates[mod, ...]
    Convenience shortcut for:

        ResolvedModelExpression[mod, Global`SBMLKineticLaws /. mod, ...]


Examples::
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000309\";
    mod = ReadModel[modUrl];
    ResolvedModelExpression[mod, Global`SBMLKineticLaws /. mod] === ResolvedReactionRates[mod]
"

PrettyPrintReactions::usage = "PrettyPrintReactions[mod]
    Lists every `mod` reaction with an id, a standard chemical reaction form,
    and a corresponding resolved rate law in a `TableForm`.


Options::
    `ResolvedModelExpression` options, e.g. AssignmentRules.


Examples::
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\";
    mod = ReadModel[modUrl];
    Global`SBMLReactions /. mod // TableForm
    PrettyPrintReactions[mod]
    PrettyPrintReactions[mod, AssignmentRules -> True]
"

RreComponents::nsm = "[ERROR] Model has no SBMLStoichiometryMatrix defined. "<>
                     "Try to read model using option: "<>
                     "return -> {SBMLStoichiometryMatrix -> True} ."
RreComponents::nspec = "[INFO] The following species are ignored due to not being reactant in any reaction: `1`."
RreComponents::nreac = "[INFO] The following reactions are ignored due to not having any reactants: `1`."
RreComponents::usage = "RreComponents[mod]
    Returns components of the reaction rate equations (RRE) for the CRN SBML
    model with the `SBMLStoichiometryMatrix` read. The components are:
    null rows and null columns reduced stoichiometry matrix, variables vector,
    reactions rates vector, and headings pair for species and reactions.


Options::
    RemoveUnconnectedSpecies, RemoveUnconnectedReactions -> True
        Flags indicating wether to remove species or reactions corresponding to
        null rows or columns respectively.
    `ResolvedReactionRates` options, e.g. InitialAssignments.


Examples::
    (* errors *)
    RreComponents[ReadModel[#]] & /@ {
      \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000020\",
      \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\"
    }

    (* actual example *)
    modUrl = \"http://www.ebi.ac.uk/compneur-srv/biomodels-main/download?mid=BIOMD0000000312\";
    mod = ReadModel[modUrl, context -> \"m\", return -> {Global`SBMLStoichiometryMatrix -> True}];

    On[Assert]; {sm, xv, vv, headings} = RreComponents[mod]; Off[Assert];
    TableForm[sm, TableHeadings -> headings]

    (* sanity checks *)
    Thread[D[#, t] & /@ xv == sm.vv] // TableForm
    (Global`SBMLODES /. mod) // TableForm

    TableForm[#1, TableHeadings -> #4] & @@ RreComponents[mod, RemoveUnconnectedSpecies -> False]
    TableForm[Global`SBMLStoichiometryMatrix /. mod, TableHeadings -> {SpeciesIds[mod], ReactionsIds[mod]}]
"



Begin["`Private`"]
(* Begin Private Context *)

(* CONSTANTS *)
(*
Molar = Units`Mole/Units`Liter (* FIXME: Mole/Meter^3 ? *)
*)

(* PRV *)
(* symbol & expr string-like replacements *)
strFun[f_,args__] := f @@ (ToString /@ {args})

subStrPrefix[sym_String, prefOut_String, prefIn_String] :=
    StringReplace[sym, StartOfString ~~ prefOut -> prefIn]
subSymPrefix[sym_,prefOut_,prefIn_] := Symbol[strFun[subStrPrefix,sym,prefOut,prefIn]]
rmSymPrefix[sym_,pref_] := subSymPrefix[sym,pref,""]
Attributes[rmSymPrefix] = {Listable}
addSymPrefix[sym_,pref_] := subSymPrefix[sym,"",pref]
Attributes[addSymPrefix] = {Listable}

startsWith[str_String,pref_String] :=
    StringMatchQ[str, StartOfString ~~ pref ~~ __]
startsWith[sym_,pref_] := strFun[startsWith,sym,pref]
addOrSubSymPrefix[sym_String,prefOut_String,prefIn_String] :=
    If[startsWith[sym,prefOut],
        subSymPrefix[sym,prefOut,prefIn],
        addSymPrefix[sym,prefIn]
    ]
(* to avoid multiple ToString conversion of the same symbols *)
addOrSubSymPrefix[sym_,prefOut_,prefIn_] :=
    strFun[addOrSubSymPrefix,sym,prefOut,prefIn]

sysFunFix = {
    System`D -> Global`\[DoubleStruckCapitalD] (*Subscript["D", ""]*),
    System`N -> Global`\[DoubleStruckCapitalN] (*Subscript["N", ""]*)
(*    ,
    Combinatorica`M -> Global`\[DoubleStruckCapitalM] (*Subscript["M", ""]*),
    Combinatorica`V -> Global`\[DoubleStruckCapitalV] (*Subscript["V", ""]*)
*)
}
sysSymFix = {
	System`E -> Global`\[DoubleStruckCapitalE] (*Subscript["E", ""]*),
	System`I -> Global`\[DoubleStruckCapitalI] (*Subscript["I", ""]*),
    System`Pi -> Global`\[DoubleStruckCapitalP]i (*Subscript["Pi", ""]*)
}
subExprInfix[expr_String,infixOut_String, infixIn: ""|"Global`"|"System`"] :=
((*Print["subExprInfix[ ",expr,", ",infixOut,", ",infixIn," ]"];*)
    (* temporary Hold to avoid evaluation of the function symbols *)
    ReleaseHold[
    	ToExpression[StringReplace[expr, {infixOut -> infixIn}], InputForm, Hold]
    	/. sysFunFix
	] /. sysSymFix
)
subExprInfix[expr_String,infixOut_String,infixIn_String] :=
((*Print["subExprInfix[ ",expr,", ",infixOut,", ",infixIn," ]"];*)
    ToExpression[StringReplace[expr, {infixOut -> infixIn}], InputForm]
)
subExprInfix[expr_,infixOut_,infixIn_] :=
	(* OutputForm is default for ToString, hence InputForm is crucial for cases
	   such as fractions (cf. ToString[OutputForm[1/x]]) *)
    strFun[subExprInfix, InputForm[expr], infixOut, infixIn]
rmExprInfix[expr_,infix_] := subExprInfix[expr,infix,""]
Attributes[rmExprInfix] = {Listable}

(* MathSBML *)
mathSbmlCtx = "Global`";

addMathSbmlCtx[s_Symbol] :=
    Module[{ctx},
        ctx = Context[Evaluate@s];
        If[ctx == mathSbmlCtx, s, addOrSubSymPrefix[s,ctx, mathSbmlCtx]]
    ]
addMathSbmlCtx[r_Rule] :=
        (* N.B. no recursive call on RHS so for options like `return` use
           `addMathSbmlCtx` explicitly to avoid using a prv context,
           e.g. addMathSbmlCtx[SBMLStoichiometryMatrix]
        *)
    	addMathSbmlCtx[First@r] -> Last@r
addMathSbmlCtx[x_] := x
Attributes[addMathSbmlCtx] = {Listable}



mathSbmlModelKeys = {
    symAlgebraicRules,
    symAssignmentRules,
    symBoundaryConditions,
    symCompartments,
    symConstants,
    symConstraints,
    symConstraintRules,
    symContext,
    symEvents,
    symFunctions,
    symIC,
    symInitialAssignments,
    symKineticLaws,
    symLevelVersion,
    symModelid,
    symModelName,
    symModelVariables,
    symNameIDAssociations,
    symNumericalSolution,
    symODES,
    symParameters,
    symReactions,
    symSpecies,
    symSpeciesCompartmentAssociations,
    symSpeciesTypes,
    symSpeciesTypeAssociations,
    symStoichiometryMatrix,
    symUnitAssociations,
    symUnitDefinitions
} = addMathSbmlCtx /@ {
    SBMLAlgebraicRules,
    SBMLAssignmentRules,
    SBMLBoundaryConditions,
    SBMLCompartments,
    SBMLConstants,
    SBMLConstraints,
    SBMLConstraintRules,
    SBMLContext,
    SBMLEvents,
    SBMLFunctions,
    SBMLIC,
    SBMLInitialAssignments,
    SBMLKineticLaws,
    SBMLLevelVersion,
    SBMLModelid,
    SBMLModelName,
    SBMLModelVariables,
    SBMLNameIDAssociations,
    SBMLNumericalSolution,
    SBMLODES,
    SBMLParameters,
    SBMLReactions,
    SBMLSpecies,
    SBMLSpeciesCompartmentAssociations,
    SBMLSpeciesTypes,
    SBMLSpeciesTypeAssociations,
    SBMLStoichiometryMatrix,
    SBMLUnitAssociations,
    SBMLUnitDefinitions
}



RmModelContextFromExpr[mod_?ModelQ, expr_] :=
        rmExprInfix[expr, symContext /. mod]
AddModelContextToSym[mod_?ModelQ, sym_] :=
        addSymPrefix[rmSymPrefix[sym,Context[sym]], symContext /. mod]



ReadModel[url_?Utilz`URLQ, opts:OptionsPattern[]] :=
    Module[{modPath, urlSaved, ret},
        modPath = CreateTemporary[];
        urlSaved = Utilz`ApplyOptSafe[URLSave, url, modPath, opts, "ReadTimeout" -> 3];
        (* context safe filtering of MathSBML options in the non-url variant *)
        ret = If[urlSaved === $Failed, urlSaved, ReadModel[modPath, opts]];
        DeleteFile[modPath];
        ret
    ]
ReadModel[filePath_?FileExistsQ, opts:OptionsPattern[]] :=
    Module[{ctx, mod, coptl},
        (* essentially SBMLRead .. *)
        (* + context shenanigans (MathSBML relies on "Global`"  *)
        ctx = $Context;
        $Context = mathSbmlCtx;
        coptl = addMathSbmlCtx@{opts};
        (* + fail instead of abort shenaningans (MathSBML aborts, instead of failing) *)
        mod = CheckAbort[
        	Utilz`ApplyOptSafe@@Join[{MathSBML`SBMLRead,filePath},coptl],
        	$Failed
    	];
        $Context = ctx;
        (*
        If[mod =!= $Failed,
        (* + faster acess to rules which are essentially used as a dictionary,
             i.e. key -> val pairs *)
        mod = Dispatch[mod];
        ];*)
        mod
    ]
ReadModel[__] := $Failed



ExpectedModelKeys = {
    symAlgebraicRules,
    symAssignmentRules,
    (*symBoundaryConditions,*)
    (*symCompartments,*)
    symConstants,
    (*symConstraints,*)
    (*symConstraintRules,*)
    symContext,
    symEvents,
    symFunctions,
    symIC,
    (*symInitialAssignments,*)
    symKineticLaws,
    symLevelVersion,
    symModelid,
    symModelName,
    symModelVariables,
    symNameIDAssociations,
    (*symNumericalSolution,*)
    symODES,
    symParameters,
    symReactions,
    symSpecies
    (*symSpeciesCompartmentAssociations,*)
    (*symSpeciesTypes,*)
    (*symSpeciesTypeAssociations,*)
    (*symUnitAssociations,*)
    (*symUnitDefinitions*)
}
ContainsExpectedModelKeysQ[keys_List] :=
    Module[{expectedKeys, hasExpectedKeys},
        expectedKeys = Complement[ExpectedModelKeys, keys];
    	hasExpectedKeys = (expectedKeys==={});
        If[!hasExpectedKeys, Message[ModelQ::nkey,expectedKeys]];
    	(* Remark: an micro optimization of caching would include sorting of
    	       the keys; MathSBML does that by default when reading SBML, but
    	       that can be disrupted by adding new keys. *)
    	MathSbmlUtilz`ContainsExpectedModelKeysQ[keys] = hasExpectedKeys;
    	hasExpectedKeys
    ]

ModelQ[expr:{__Rule}] := ContainsExpectedModelKeysQ[First /@ expr]
ModelQ[expr_] := False

(* opt: RreModelQ which checks if indeed SBMLODES are equal to S v(x) *)
CrnModelQ[mod_?ModelQ] := Length[symReactions /. mod] > 0
CrnModelQ[expr_] := False

IrreversibleReactionQ[expr_] :=
    StringQ[expr] && (StringPosition[expr,"\[RightArrowLeftArrow]"] === {})
(*
AllIrreversibleQ[mod_?ModelQ] :=
    Fold[And, True, IrreversibleReactionQ /@ (symReactions /. mod)]
*)

ModelConstantQ[mod_?ModelQ] :=
    Function[{var},
        MemberQ[First /@ (symConstants/.mod), AddModelContextToSym[mod,var]]
    ]
ModelParameterQ[mod_?ModelQ] :=
    Function[{var},
        MemberQ[(symParameters/.mod), ToString[AddModelContextToSym[mod,var]]]
    ]
ModelSymbolicParameterQ[mod_?ModelQ] :=
    With[{parQ=ModelParameterQ[mod],constQ=ModelConstantQ[mod]},
        Function[{var}, parQ[var] && Not[constQ[var]] ]
    ]




ModelKeys[mod_?ModelQ] := First /@ mod
(*
ModelKeys[mod:{__Rule}] := First /@ mod
ModelKeys[mod_Dispatch] := ModelKeys[First[mod] /. Dispatch -> List]
*)



headStr = Composition[ToString,Head]
lhsHeadStr = Composition[headStr,First]
(* works for species, reaction rates or initial condition *)
extractModId[mod_?ModelQ,extractIdFun_] :=
    Composition[RmModelContextFromExpr[mod,#]&,extractIdFun]

(* FIXME messy Mathematica-safe to String and back conversions; make clean API for that *)
strSymFix =
    (subStrPrefix[ToString[Last[#]],mathSbmlCtx,""]->ToString[First[#]])& /@
        Join[sysFunFix,sysSymFix]
fixStrId[id_] := StringReplace[ToString[id], strSymFix]
Attributes[fixStrId] = {Listable}

ReactionsIds[mod_?ModelQ] :=
    fixStrId @ (extractModId[mod,lhsHeadStr] /@ (symKineticLaws/.mod))
SpeciesIds[mod_?ModelQ] :=
    fixStrId @ (extractModId[mod,headStr] /@ (symSpecies/.mod))



GetConstantValue[mod_?ModelQ, par_] :=
    (AddModelContextToSym[mod, par] /. (symConstants /. mod)) /; ModelConstantQ[mod][par]
GetConstantValue[mod_?ModelQ, par_] :=
    (Message[ModelConstantQ::fail, par]; $Failed)

(* OPT if possible re-write these to use current expr/sym/str replacement funs *)
setParExpr[mod_, var_ -> val_] :=
    Module[{posl,ret},
        (* Mathematica SBML model is a List of Symbol->List or String elements
           thus, when matching symbols, Position matching symbol can return only
           first case elements, i.e.:
              {SBMLx, list, item, [itemLHS, itemRHS, ...] }
           We want to elminate those where itemLHS == 1 or it does not exist
           (e.g. on SBMLParameters list). For remaining positions substitute
           them with given constant value.
        *)
        posl = Select[Position[mod, var], Length[#] > 3 && #[[4]] != 1 &];
        ret = ReplacePart[mod, posl -> val];
        ret
    ]
setParStr[mod_, var_ -> val_] :=
    Module[{varstr, strposl},
        varstr = ToString[var];
        strposl = Position[mod, x_String /; StringMatchQ[x,__~~varstr~~___]];
        (* non-empy suffix guarantees that we're not replacing lhs of
           rule represented by string *)
        Fold[Function[{ml,i}, Module[{iv},
            iv=ml[[##]]&@@i;
            ReplacePart[ml, i->StringReplace[iv, varstr -> ToString[val]]]
        ]], mod, strposl]
    ]
SetSymbolicParameterValue[mod_?ModelQ, par_, val_?NumericQ] :=
(*SetSymbolicParameterValue[mod_, par_, val_?NumericQ] :=*)
    Module[{var, rul, ret, consts},
        var = AddModelContextToSym[mod, par];
        rul = var -> val;

        (* expr replace, then string replace *)
        ret = setParStr[setParExpr[mod, rul], rul];
        (* String replace, e.g. for events internal representation *)
        (* Finally, update the SBMLConstants *)
        consts = DeleteDuplicates[Prepend[symConstants /. ret, rul],
                                  #1[[1]] == #2[[1]] &];
        ret /. (symConstants -> {___}) -> (symConstants -> consts)
    ] /; ModelSymbolicParameterQ[mod][par]
SetSymbolicParameterValue[mod_?ModelQ, par_, val_?NumericQ] :=
    (Message[ModelSymbolicParameterQ::fail, par]; $Failed)



(* just a little more robust solution than just checking for {} *)
noInitalAssignmentsQ[mod_] := With[{ias=symInitialAssignments/.mod},
	   (ias===symInitialAssignments) || Utilz`EmptyListQ[ias]
    ]

GetInitialAssignments[mod_?ModelQ] := {} /; noInitalAssignmentsQ[mod]

(* Fix: rm the LHS [0] index from params, and leave it for ICs *)
rmParIaIdx[mod_] :=
    With[{parQ = ModelSymbolicParameterQ[mod]},
        Function[ {ias}, ias /.((x_?parQ[0]->rhs_)->(x->rhs)) ]
    ]
GetInitialAssignments[mod_?ModelQ] :=
    rmParIaIdx[mod][symInitialAssignments/.mod]

(* reFix: add back [0] idx where its missing *)
addIaIdx[ias_] := ( ias /.  ((x_Symbol->rhs_)->(x[0]->rhs)) )

(* micro-optimization; should work w/o this as well *)
ResolveInitialAssignments[mod_?ModelQ]:= mod /; noInitalAssignmentsQ[mod]

(* Self-resolve rules RHSs until no change *)
SelfResolve[r:{___Rule}] := Thread[(First/@r) -> ((Last/@r)//.r)]

numResolvedIaQ[var_ -> val_] := NumericQ[val]
Options[ResolveInitialAssignments] := {
    MoveNotNumeric -> False
}
ResolveInitialAssignments[mod_?ModelQ, OptionsPattern[]] :=
    Module[{
    	consts,
    	iasResolved, setIa, modResolved,
    	iasMove, iasConsts, iasICs,
    	iasNew, constsNew, icNew
	},

        consts = symConstants/.mod;

        (* Self-resolve IAs *)
        iasResolved=SelfResolve[GetInitialAssignments[mod] /.consts];
        (* ? use instead existing replacement API:
            ias = symInitialAssignments /. mod;
            iasResolved = Thread[(First/@ias) -> ResolvedModelExpression[mod, Last /@ ias, Consts -> True, InitialAssignments -> True]];
        *)

        (* Apply resolved IAs to the model *)
        setIa=setParStr[setParExpr[#1,#2],#2]&;
        modResolved=Fold[setIa,mod,iasResolved];

        (* OPT: comment out *)
        Assert[Block[{ret},
        	Off[First::first,Last::nolast];
        	ret = Thread[First@#1==First@#2 && Last@#1==Last@#2 & [
	        	(* substitutions made directly in the model via setIa *)
	        	rmParIaIdx[mod][(symInitialAssignments/.modResolved)/.consts],
	        	(* substitution rules *)
	        	iasResolved
    	    ]];
    	    On[First::first,Last::nolast];
    	    ret
        ]];


        (* split resolved IAs to new consts, new ICs or to stay *)
        iasMove=If[OptionValue[MoveNotNumeric], iasResolved,
        	Select[iasResolved,numResolvedIaQ]];
        (* taking advantage of rm'ed [0] to detect which IAs go to constants *)
        iasConsts = Cases[iasMove,HoldPattern@(_Symbol->_)];
        (* remaining IAs go to ICs; swap here -> to == *)
        iasICs = Complement[iasMove,iasConsts]/.Rule->Equal;
        (* MathSBML compatibility: add back [0] idx where its missing *)
        iasNew=addIaIdx[Complement[iasResolved,iasMove]];

        {constsNew,icNew} = Utilz`UnionBy @@@ {
        	{iasConsts,consts},
        	{iasICs, symIC/.modResolved}
    	};
    	(* remark: order in consts and ICs lists is not preserved; seems irrelevant *)
    	(* OPT def SetICValue funcion and use it here *)

        modResolved/.(
            ((#[[1]] -> {___}) -> (#[[1]] -> #[[2]]))&/@
                {{symInitialAssignments,iasNew},{symConstants,constsNew},{symIC,icNew}}
        )
    ]


(* OPT full-fetched ResolveFunctions and ResolveAssignmentRules, analogously to
       ResolveInitialAssignments, i.e. return model w/ lists re-arrangements
*)



(* Return rules instead of equalities; LHS is always a single variable *)
GetAssignmentRules[mod_?ModelQ] :=
    ((symAssignmentRules /. mod) /. Equal -> Rule)

Options[ResolvedModelExpression] := {
    InitialAssignments -> False,
    AssignmentRules -> False,
    Consts -> False,
    Functions -> True
}
ResolvedModelExpression[mod_?ModelQ, expr_, OptionsPattern[]] := (
    expr //. Join[
        If[OptionValue[InitialAssignments],	GetInitialAssignments[mod], {}],
        If[OptionValue[AssignmentRules], GetAssignmentRules[mod], {}],
        If[OptionValue[Consts], symConstants /. mod, {}],
        If[OptionValue[Functions], symFunctions /. mod, {}]
    ]
)

Options[ResolvedReactionRates] = Options[ResolvedModelExpression]
ResolvedReactionRates[mod_, opts: OptionsPattern[]] :=
    ResolvedModelExpression[mod, symKineticLaws /.mod, opts]



Options[PrettyPrintReactions] := Options[ResolvedModelExpression]
PrettyPrintReactions[mod_?ModelQ, opts:OptionsPattern[]] :=
    (* ??? Before removing context add '*' before digit symbols - what was that about ? *)
    (* OPT For multimers add subscript  *)
    (RmModelContextFromExpr[mod, #] & /@
        {lhsHeadStr@#[[1]], #[[2]], Last@#[[1]]} & /@
            (* no opts filtering since we pass all of them to one fun *)
            Transpose[{
                ResolvedReactionRates[mod, opts],
                symReactions /.mod
            }]
    ) // TableForm



Options[RreComponents] = Utilz`OptsUnion[{
    RemoveUnconnectedSpecies -> True,
    RemoveUnconnectedReactions -> True
}, Options[ResolvedReactionRates]]
RreComponents[mod_?CrnModelQ, opts:OptionsPattern[]] :=
    Module[{sm, xv, vv, xid, vid, smRet, xvRet, vvRet, xidRet, vidRet},
        sm = symStoichiometryMatrix /. mod;
        xv = symSpecies /. mod;
        vv = Last /@ Utilz`ApplyOptSafe[ResolvedReactionRates, mod, None, opts];
        xid = SpeciesIds[mod];
        vid = ReactionsIds[mod];

        Assert[Dimensions@sm == Length/@{xv,vv} == Length/@{xid,vid}];

        (* eliminate and report zero rows (e.g. for input / free variables), and
           zero columns (that is some pathology, right?) *)
        {sm, xvRet, xidRet} = If[OptionValue[RemoveUnconnectedSpecies],
            Utilz`RemoveNullRows[sm, xv, xid],
            {sm, xv, xid}
        ];
        If[Length[xv] != Length[xvRet],
            Message[RreComponents::nspec, Complement[xid,xidRet]]
        ];
        {sm, vvRet, vidRet} = If[OptionValue[RemoveUnconnectedReactions],
            Utilz`RemoveNullRows[Transpose[sm], vv, vid],
            {Transpose[sm], vv, vid}
        ];
        If[Length[vv] != Length[vvRet],
            Message[RreComponents::nreac, Complement[vid, vidRet]]
        ];
        smRet = Transpose[sm];

        {smRet, xvRet, vvRet, {xidRet, vidRet}}
    ] /; MatrixQ[symStoichiometryMatrix /. mod]
RreComponents[mod_?CrnModelQ] := (
    Message[RreComponents::nsm]; $Failed
)
RreComponents[mod_?ModelQ] := (
    Message[CrnModelQ::fail]; $Failed
)



End[] (* End Private Context *)

EndPackage[]
