(* ::Package:: *)

(* TODO:
	1. trim to functions actually used in the package;
	2. add missing docstrings
*)

(* Mathematica Package *)
BeginPackage["Utilz`"]
(* Exported symbols added here with SymbolName::usage *)

(********************************** MSGS **************************************)
UtilzError::ish = "Incompatible shapes.`1`"
UtilzError::nad = "Directory not available: `1`."


(********************************** MATH **************************************)
PiecewiseLinear::usage = "PiecewiseLinear[{{\!\(\*SubscriptBox[
StyleBox[\"x\", \"TI\"],
StyleBox[\"1\", \"TR\"]]\),\!\(\*SubscriptBox[
StyleBox[\"f\", \"TI\"],
StyleBox[\"1\", \"TR\"]]\)},{\!\(\*SubscriptBox[
StyleBox[\"x\", \"TI\"],
StyleBox[\"2\", \"TR\"]]\),\!\(\*SubscriptBox[
StyleBox[\"f\", \"TI\"],
StyleBox[\"2\", \"TR\"]]\)},\!\(\*
StyleBox[\"\[Ellipsis]\", \"TR\"]\)}]
    constructs an piecewise linear interpolation of the function values \!\(\*SubscriptBox[
StyleBox[\"f\", \"TI\"],
StyleBox[\"i\", \"TI\"]]\)
    corresponding to \!\(\*
StyleBox[\"x\", \"TI\"]\) values \!\(\*SubscriptBox[
StyleBox[\"x\", \"TI\"],
StyleBox[\"i\", \"TI\"]]\).

    Source: http://goo.gl/mZtyG"
PiecewiseStep::usage = "PiecewiseStep[{{\!\(\*SubscriptBox[
StyleBox[\"x\", \"TI\"],
StyleBox[\"1\", \"TR\"]]\),\!\(\*SubscriptBox[
StyleBox[\"f\", \"TI\"],
StyleBox[\"1\", \"TR\"]]\)},{\!\(\*SubscriptBox[
StyleBox[\"x\", \"TI\"],
StyleBox[\"2\", \"TR\"]]\),\!\(\*SubscriptBox[
StyleBox[\"f\", \"TI\"],
StyleBox[\"2\", \"TR\"]]\)},\!\(\*
StyleBox[\"\[Ellipsis]\", \"TR\"]\)}]
    constructs an piecewise step interpolation of the function values \!\(\*SubscriptBox[
StyleBox[\"f\", \"TI\"],
StyleBox[\"i\", \"TI\"]]\)
    corresponding to \!\(\*
StyleBox[\"x\", \"TI\"]\) values \!\(\*SubscriptBox[
StyleBox[\"x\", \"TI\"],
StyleBox[\"i\", \"TI\"]]\). Resulting function is left-continuous."

ColumnSpace::usage = "ColumnSpace[matrix]
    See: http://en.wikipedia.org/wiki/Column_space#Basis

Examples::
    (m = {{1, -1}, {1, -1}, {-1, 1}}) // MatrixForm
    (c = ColumnSpace[m]) // MatrixForm
    (* column space is an orthogonal complement of the left null space *)
    (l = NullSpace[Transpose[m]]) // MatrixForm
    l.c == ConstantArray[0, {Length@l, Length@First@c}]
"

RemoveNullRows::usage = "RemoveNullRows[m]
    Removes from matrix `m` rows which are null vectors (cf. `NotNullVectorQ`).


RemoveNullRows[m, expr1, ...]
    Removes from expresions expr1, ... positions corresponding to all null rows
    of a matrix `m`. Each of the expressions is required to have length equal
    to the number of rows in `m`.

    Returns list containing reduced matrix and expressions.


Examples::
    (m = {{0, 0, 0}, {0, 1, -1}, {0, -1, 1}, {0, 0, 0}}) // MatrixForm
    rowN = Range[Length[m]]
    {rm, rrowN} = RemoveNullRows[m, rowN]
    (* reduced matrix *)
    rm // MatrixForm
    (* numbers of removed rows *)
    Complement[rowN, rrowN]
    (* also reduce by columns; Note: only 1 arg, so we get a matrix instead of list w/ one at first position *)
    Transpose[RemoveNullRows[Transpose[rm]]] // MatrixForm


    (* works only for matrices *)
    RemoveNullRows /@ {{}, {{}}}


    (* fails if dimension-incompatible arguments are provided *)
    Dimensions[{{}}]
    RemoveNullRows[{{}}, #] & /@ {{1}, {}} // MatrixForm
"


(********************************** LIST **************************************)
PlusList::usage = "PlusList[list]
    gives sum of the elements of the list as applied by the Plus function."
CumSum::usage = "CumSum[list]
    Returns a list whose elements are the cumulative sums of the argument."
Pairs::usage = "Pairs[list,list]
    gives list of pairs of coresponding elements in both lists. Argument lists
    have to be of equal length."
PadRows::usage = "PadRows[{list, list, ...}]
    cyclically pads lists (array rows) on the input list (array) to the right,
    to the length of the longest list.
    "
Repeat::usage = "Repeat[x, n]
    gives list of length n with elements x. Shortcut for the built-in
    Table[x,{n}] call.
    "
ZipLists::usage = "ZipLists[list1,list2,...], ZipLists[{list1,list2,...}]
    gives list of tuples, where each tuple contains the i-th element from each
    of the argument lists. Argument lists are first cyclically padded right to
    have a equal length.
    "
SingletonOrId::usage = "SingletonOrId[x]
    returns identity if x is a List, list with x as the only element (singleton)
    otherwise.
    "
SimplifySingleton::usage = "SimplifyList[expr]
    returns list element if expr is a singleton list, Null if expr is an empty
    list and expr otherwise.
    "
Tail::usage = "Tail[list]
    returns list[[2;;]] or empty list if list is empty. Similar to built-in
    functions Part and Rest. For not empty list it is equivalent to
    Part[list,2;;] and Rest[list].
    "

UnionBy::usage = "UnionBy[list1, ..., listN, ?property]
    Like `Union` gives list of all the distinct elements that appear in any of
    `list1`, ..., `listN`. Unlike `Union` does not sort. Moreover, lists are
    merged according to `property` of their elements. By default this is a first
    part of each element.


Examples::
    UnionBy[{a, b}, {b, c}, # &] === Union[{a, b}, {b, c}] =!= UnionBy[{c, b}, {b, a}, # &]

    UnionBy[{x == 1, y == 1}, {x == 2, y == 2}]

    UnionBy[{x == 1, y == 1}, {x == 2, y == 2}]
    UnionBy[{x == 1, x -> 2, {x, 3 }}]
    UnionBy[{x == 1, a -> 1}, {x -> 2, b -> 2}, {{x, 3}, c -> 3}]
    UnionBy[{x, 1}, {x, 2}]

    UnionBy[{x + y -> sum, x + x -> sum}, Part[#, 2] &]
"


(********************************** TEST **************************************)
URLQ::usage = "URLQ[expr]
    gives `True` if `expr` is a valid URI (according to RFC 3986), and `False`
    otherwise. Requires giving both scheme (e.g. \"http://\") and authority
    (e.g. \"example.org\") elements of the URI.
    Beware: since it is URI, not URL, invalid URLs can pass this test.

    Regular expression src: http://tools.ietf.org/html/rfc3986#page-50

Examples::
    URLQ /@ {
        \"http://www.example.org/\",
        \"http://www.example.org:80/\",
        \"example.org\",
        \"ftp://user:pass@example.org/\",
        \"http://example.org/?cat=5&test=joo\",
        \"http://www.fi/?cat=5&amp;test=joo\",
        \"http://[::1]/\",\"http://[2620:0:1cfe:face:b00c::3]/\",
        \"http://[2620:0:1cfe:face:b00c::3]:80/\",
        \"\"
    }
"
EmptyListQ::usage = "EmptyListQ[expr]
    gives `True` if `expr` is an empty list, and `False` otherwise.

Examples::
    Cases[#, Except[a]] & /@ {{a, a}, {a, b}}
    EmptyListQ /@ %
"
NotNullVectorQ::usage = "NotNullVectorQ[vec]
    gives `True` if `vec` satisfies `VectorQ` and has at least one non zero
    element, else `False`. In particular, an empty vector is also a null vector.

Examples::
    NotNullVectorQ /@ {Symbol, {}, {0,0}, UnitVector[2]}
"
If[Not[ValueQ[SquareMatrixQ[{}]]], (* New in Mathematica 10 *)
SquareMatrixQ::usage = "SquareMatrixQ[m]
    gives `True` if `m` satisfies `MatrixQ` and has same `Dimensions`, else
    `False`.

Examples::
    SquareMatrixQ /@ {Symbol, {}, {{}}, ConstantArray[0,{1,1}], ConstantArray[0,{1,2}]}
"
]
PositiveIntegerQ::usage = "PositiveIntegerQ[expr]
    gives True if expr is a positive integer, and False otherwise.

    See IntegerQ for details.
    "
NonNegativeIntegerQ::usage = "NonNegativeIntegerQ[expr]
    gives True if expr is a non negative integer, and False otherwise.

    See IntegerQ for details.
    "
PositiveNumberQ::usage = "PositiveNumberQ[expr]
    gives True if expr is a positive number, and False otherwise.

    See NumberQ for details.
    "
NonNegativeNumberQ::usage = "NonNegativeNumberQ[expr]
    gives True if expr is a non negative number, and False otherwise.

    See NumberQ for details.
    "
FunctionQ::usage = "FunctionQ[expr]
    gives True if expr is a (non-pathological) function, and False otherwise.
    Not sound so use carefully. Sample built-in functions that do not pass this
    test: First, Last, Rule.

    See http://tinyurl.com/yl43qrz for details.
    "



(********************************** OPTS **************************************)
ApplyOptSafe::usage = "ApplyOptSafe[f, arg1, ..., argN, opt1, ..., optM]
    Applies `f` function to `arg1`, ..., `argN` arguments and `opt1`, ...,
    `optM` which are only relevant to `f` (filtered via `FilterRules`).

    Remark: arguments are not checked for not being an option.

    Beware: if you want to pass arguments that are not options but they do match
        `OptionsPattern[]` use dummy argument `None` to separate arguments from
        options.

Examples::
    opts = Sequence[PlotStyle -> Red, CustomOption -> True, PlotStyle -> Green];
    args = Sequence[Sin[x], {x, -3, 3}];
    Plot[Evaluate@args, Evaluate@opts] (* remark: turning Off[Plot::optx] will not help *)
    ApplyOptSafe[Plot, args, opts]


    Options[fun] := { FunOpt -> 1 }
    fun[x___, opts : OptionsPattern[fun]] := {{x}, {opts}}
    nopt = { NOpt -> 0 };
    opts = Sequence[FunOpt -> 2, BlahOpt -> 1];
    ApplyOptSafe[fun, nopt, opts ]
    ApplyOptSafe[fun, nopt, None, opts]
"
OptsSeq::usage = "OptsSeq[funName]
    is a wrapper for Options[funName] but returns Sequence instead of List."
OptsMinus::usage = "OptsMinus[optsList, optsNamesOrRulesList]
    removes from list of options pattern options named in optsNamesOrRulesList.
    When given a options pattern as a second argument only the rhs of rules is
    ignored."
(* TODO this seems deprecated - i.e. it looks like this is how options now natively work *)
OptsUnion::usage = "OptsUnion[{OptionsPattern[]}, {OptionsPattern[]}, ...]
    unions the options pattern lists by name in order preference, e.g.
        OptsUnion[{a->1},{a->2,b->1,c->{d->1}},{b->2,c->{e->1}}] == {a->1,b->1,c->{d->1}}

    Note: union is done only at the first level of options.
    "
OptsDefaults::usage = "OptsDefaults[optionsArg, opts]
    returns the opts suplemented with missing default values according to
    Options function argument optionsArg; e.g.
        OptsDefaults[Plot, Mesh -> Full]
    will return Plot function options with Mesh option equal to Full.
    "
optsNames::usage = "temp"



(********************************** IN/OUT ************************************)
(*    Remark: writes in the `StandardForm`.*)
LogOutput::usage = "LogOutput[logPath, f, arg1, ...]
    Log print output and messages from the `f[arg1, ...]` call into the
    `logPath` file, in addition to the `$Messages` and `$Output` streams.
    Attention: overwrites the file.

Examples::
    logPath = CreateTemporary[];
    LogOutput[logPath, 1./Range[-#, #]&, 5]
    FilePrint[logPath]
    DeleteFile[logPath];
"



AddTableHeadings::usage = "AddTableHeadings[tab, { rowHeadings, colHeadings }, ?rowNamesHead]
    Prepend to a table `tab` rows and columns names, with an optional name
    for the row names column.

Examples::
    m = Array[a, {2, 5}];
    ExportString[AddTableHeadings[m, {Range[2], Range[5]}, \"ID\"], \"CSV\"]

    AddTableHeadings[m, {Range[2], Range[3]}]
"


Begin["`Private`"]
(* Begin Private Context *)


(********************************** MATH **************************************)
dataSeriesPattern := {{_?NumberQ, _?NumberQ..}..}
(*piecewisePairs[points: dataSeriesPattern, pairf_]:= Module[
{pp:=points, pairs},
pairs := (pairf/@ Partition[pp, 2, 1]);
Return[Function[x,
    Evaluate[Piecewise[
      {
        InterpolatingPolynomial[#1, x],
        x >= #1[[1]][[1]] && x < #1[[2]][[1]]
      } & /@ pairs
    ]]
]]
]*)
PiecewiseLinear[points: dataSeriesPattern]:= (*piecewisePairs[points, Function[{x},x]]*)
Module[
{pp:=points, pairs},
pairs := (Partition[pp, 2, 1]);
Return[Function[x,
    Evaluate[Piecewise[
      {
        InterpolatingPolynomial[#1, x],
        x >= #1[[1]][[1]] && x < #1[[2]][[1]]
      } & /@ pairs,
      Indeterminate
    ]]
]]
]
PiecewiseStep[points: dataSeriesPattern]:=
(*piecewisePairs[points, {#[[1]],{#[[2]][[1]],#[[1]][[2]]}}&]*)
    Module[
      {pp:=points, pairs},
      pairs := ({#[[1]],{#[[2]][[1]],#[[1]][[2]]}}& /@ Partition[pp, 2, 1]);
      Return[Function[x,
        Evaluate[Piecewise[
          Join[
            (*{{First[pp][[2]], x < First[pp][[1]]}},*)
            {
              InterpolatingPolynomial[#1, x],
              x >= #1[[1]][[1]] && x < #1[[2]][[1]]
            } & /@ pairs,
            {{Last[pp][[2]], x == Last[pp][[1]]}}
          ],
        Indeterminate
        ]]
      ]]
    ]
ColumnSpace[m_?MatrixQ]:=
    m[[;;,
       (* get first columns w/ leading 1 (non-zero element) *)
       Last@Transpose@DeleteDuplicates[
          Position[RowReduce[m], x_ /; x != 0, 2],
          #1[[1]] == #2[[1]] &
       ]
    ]]
nonZeroRowsIdx[m_?MatrixQ] :=
    Flatten[Position[m, _?NotNullVectorQ, {1}, Heads -> False]]
nonZeroSelectF[m_?MatrixQ] :=
    Module[{nzIdx},
        nzIdx = nonZeroRowsIdx[m];
        #[[nzIdx]] &
    ]
RemoveNullRows[m_?MatrixQ, vs__] := (
    nonZeroSelectF[m] /@ {m, vs}
) /; Fold[And[#1,(Length[#2] == Length[m])] &, True, {vs}]
RemoveNullRows::ish = " At least one subsequent argument has length different"<>
                      " than the first argument."
RemoveNullRows[m_?MatrixQ, vs__] := (
    Message[UtilzError::ish,RemoveNullRows::ish]; $Failed
)
(* for a single arg result is simplified to a matrix *)
RemoveNullRows[m_?MatrixQ] := (
    nonZeroSelectF[m] @ m
)

(********************************** LIST **************************************)
PlusList[list_List] := Plus @@ list
CumSum[list_List] := PlusList[list[[;;#]]] & /@ Range[Length[list]]
PadRows[a:{__List}] := PadRight[#, Max[Length /@ a], #] & /@ a
Repeat[x_,n_?NonNegativeIntegerQ] := Table[x,{n}]
SingletonOrId[x_] := If[x[[0]]===List, x, {x}]
Pairs[l1_List, l2_List] := ZipLists[l1,l2]
ZipLists[x:{__List}] := Transpose[PadRows[x]]
ZipLists[x__List] := ZipLists[{x}]
ZipLists[x__] := ZipLists[SingletonOrId/@{x}]
Tail[{}] = {}
Tail[l_List] := l[[2;;]]
SimplifySingleton[{}]=Null
SimplifySingleton[{x_}] := x
SimplifySingleton[x_] := x
defaultProp = (Part[#,1]&)
(* TODO FIXME: FunctionQ[Last] *)
UnionBy[l:_List, prop:_?FunctionQ:defaultProp] :=
    DeleteDuplicates[l, prop[#1] == prop[#2] &]
UnionBy[ll:__List, prop:_?FunctionQ:defaultProp] :=
    Fold[UnionBy[Join[#1,#2],prop]&,{},{ll}]

(********************************** TEST **************************************)
(* src: http://tools.ietf.org/html/rfc3986#page-50 + rm optionallity from scheme and authority *)
regexURI = RegularExpression["^(([^:/?#]+):)(//([^/?#]*))([^?#]*)(\?([^#]*))?(#(.*))?"]
URLQ[expr:_] := StringQ[expr] && StringMatchQ[expr, regexURI]
EmptyListQ[expr:_] := (expr === {})
NotNullVectorQ[expr:_] := VectorQ[expr] && (Count[Unitize@expr, 1] > 0)
If[Not[ValueQ[SquareMatrixQ[{}]]],
	SquareMatrixQ[expr:_] := MatrixQ[expr] && (SameQ @@ Dimensions[expr])
]
PositiveQ[expr_] := expr > 0
NonNegativeQ[expr_] := expr >= 0
PositiveIntegerQ[expr_] := IntegerQ[expr] && PositiveQ[expr]
NonNegativeIntegerQ[expr_] := IntegerQ[expr] && NonNegativeQ[expr]
PositiveNumberQ[expr_] := NumberQ[expr] && PositiveQ[expr]
NonNegativeNumberQ[expr_] := NumberQ[expr] && NonNegativeQ[expr]
FunctionQ[expr_] := (Head[expr]===Function) || (Head[expr] === Symbol &&
        ((DownValues[expr] =!= {}) ||
          (Intersection[Attributes[expr],{NumericFunction}] =!= {})
        ))


(********************************** OPTS **************************************)
optsNames[optsList:{OptionsPattern[]}] :=
    Cases[optsList, ((n_ :> _) | (n_ -> _) ) -> n]

argNopt = None (* must be in a globally fixed context *)
ApplyOptSafe[f_?FunctionQ, args___, argNopt, opts:OptionsPattern[]] := (
(*Print["Full:", f,"[",{args},", ",FilterRules[{opts},Options[f]],"]"];*)
    f@@Join[
        {args},
        (* apparently do not have to DeleteDuplicates[.., #1[[1]] == #2[[1]] &],
           since by default no err/warning and first occurence counts *)
        FilterRules[{opts},Options[f]]
    ]
)
ApplyOptSafe[f_?FunctionQ, args___, opts:OptionsPattern[]] := (
(*Print["Wrap:", f,"[",{args},", ",FilterRules[{opts},Options[f]],"]"];*)
    ApplyOptSafe[f, args, argNopt, opts]
)

OptsSeq[funName_] :=
Module[{opts},
    opts = Options[funName];
    opts[[0]] = Sequence;
    opts
]

OptsMinus[optsList:{OptionsPattern[]}, {}] :=
optsList
OptsMinus[optsList:{OptionsPattern[]}, optsNamesList:{__Symbol}] :=
Cases[optsList, Except[(Fold[Alternatives, False, optsNamesList]) -> _]]
OptsMinus[optsList1:{OptionsPattern[]}, optsList2:{OptionsPattern[]}] :=
OptsMinus[optsList1,optsNames[optsList2]]

OptsUnion[optsList1:{OptionsPattern[]}, optsList2:{OptionsPattern[]}] :=
UnionBy[optsList1,optsList2]
(* below sol was flawed - it left repeats within optsList1 *)
(*Union[optsList1,OptsMinus[optsList2, optsList1]]*)
OptsUnion[optsLists:Sequence[OptionsPattern[]]] :=
Fold[OptsUnion,{},{optsLists}]

OptsDefaults[optionsArg_, opts:OptionsPattern] :=
    OptsUnion[Flatten[{opts},1],Options[optionsArg]]


(********************************** IN/OUT ************************************)
(* SRC: http://mathematica.stackexchange.com/questions/29321/what-are-the-best-practices-most-common-idiomatic-ways-to-report-errors-in-m *)
(*
chainIfNotFailed[funs_List, expr_] :=
    Module[ {failException},
        Catch[Fold[
           If[ #1 === $Failed, Throw[$Failed, failException], #2[#1]] &,
           expr, funs
        ], failException]
    ];
*)
LogOutput[logPath_String,fun_?FunctionQ,args__] :=
    Module[{msgs,outs,log,ret},
        {msgs,outs} = {$Messages,$Output};
        log = OpenWrite[logPath(*, FormatType -> StandardForm*)]; (* InputForm *)
        If[log === $Failed, ret = $Failed,
            {$Messages,$Output} = Repeat[{log},2];
            (*{$Messages,$Output} = (Prepend[#,log]&/@{$Messages,$Output});*)
            ret = fun[args];
            Close[logPath]
        ];
        {$Messages,$Output} = {msgs,outs};
        ret
    ]


(* FIXME that simply does not work - sample expr_ gets apparently evaluated *)
$debug
PrintDebug::usage = "PrintDebug[expr]
    Print out `debug` `Head` calls from the wrapped `expression`.
    Attention: requires that the `debug` symbol is not overloaded.
    SRC: http://stackoverflow.com/a/5122428/207241

Options::
    `PrintPrefix` By default \"[DEBUG] \".

Examples::
    inc[i_Integer] := ($debug[\"inc[\",i,\"]\"]; i+1);
    inc /@ Range[0,10]
    inc /@ Range[0,10] // PrintDebug
"
Options[PrintDebug] = {PrintPrefix -> "[DEBUG] "}
Attributes[PrintDebug]={HoldAll}
PrintDebug[expr_,opts:OptionsPattern[]] :=
    With[{$debug = Print[OptionValue[PrintDebug,PrintPrefix],##]&}, expr]



headingsPattern = {{___}, {___}}
AddTableHeadings[tab:_?MatrixQ, headings:headingsPattern, rowNamesHead:_:""] := (
    MapThread[Prepend, {
        Prepend[tab, Last@headings],
        Prepend[First@headings, rowNamesHead]
    }]
) /; Dimensions[tab] == (Length/@headings)
AddTableHeadings::ish = " Lenghts of rows and columns headings must be equal to"<>
                        " respective matrix dimesions."
AddTableHeadings[tab:_?MatrixQ, headings:headingsPattern, _:""] := (
    Message[UtilzError::ish,AddTableHeadings::ish]; $Failed
)

End[] (* End Private Context *)

EndPackage[]
