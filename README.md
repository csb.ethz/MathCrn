Intro
=====

Set of Mathematica modules (packages) for basic qualitative analysis of chemical reaction networks (CRN): symbolic computation of Jacobians of corresponding ODEs, their sign pattern, as well as computation of the soichiometric reductions of CRN.


Quickstart
==========

If you are planning to use SBML models [[1]], get the MathSBML package (>=2.10) [[2]]. You can then, for instance, load models directly in the Mathematica Notebook from the BioModels DB [[3]].

Get the MathCrn latest release [[4]] and add the modules (`.m`) files to a folder on the Mathematica's search path (a folder listed in the `$Path`, e.g. `$UserBaseDirectory/Applications`). Alternatively, you can also do that by modifying `$Path` variable in the Notebook. In either case, have a look at Notebooks accompanying each module. You can find there list of functions with their descriptions and exampels of applications.

The `Utilz.nb` and `MathSbmlUtilz.nb` notebooks contain examples for multiple small utility funcitons, `MathCrn.nb` notebook showcases the core package functionality, and finally `SbmlReacRate.nb` contains examples for few functions for analysis of reaction rates monotonicities in SBML models.


Authors
=======

    Mikołaj Rybiński <mikolaj.rybinski@bsse.ethz.com>

Funding
=======

This project was funded by Swiss National Science Foundation grant no. 141264.

References
==========
[1]: http://sbml.org/Main_Page
1. [SBML Web site][1]
[2]: https://sourceforge.net/projects/sbml/files/mathsbml/
2. [Sourceforge: MathSBML releases][2]
[3]: https://www.ebi.ac.uk/biomodels-main/
3. [BioModels DB Web site][3]
[4]: https://gitlab.com/csb.ethz/MathCrn/tags
4. [GitLab: MathCrn releases][4]
